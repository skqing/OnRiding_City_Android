![输入图片说明](http://p17.qhimg.com/t01d4be69f4f9baeeb0.png "在这里输入图片标题")


## 介绍
这是一个用于骑行的APP，主要功能是记录骑行轨迹，显示骑行速度海拔等数据。
后来由于工作原因放弃了继续开发，现在开源了（[包括后台接口部分](http://git.oschina.net/dolphinboy/onriding-api)）作为大家一个参考项目吧。
此项目已经发布到了各大[应用市场](http://zhushou.360.cn/detail/index/soft_id/1891422?recrefer=SE_D_%E8%BD%A6%E8%BD%AE%E4%B8%8D%E6%81%AF)


## 第三方工具
用于HTTP, DAO 和 图片加载
[xUtils](https://github.com/wyouflf/xUtils)  
下拉刷新组件
[Android-PullToRefresh](https://github.com/chrisbanes/Android-PullToRefresh)


2014年3月2日
建立了新的分支，把之前的分支都转义到breadtrip上，在master上重新开发。  
要考虑两个方面：第一，要简单，好用，稳定更适合于专业骑行驴友；第二，要重SNS的作用


## 版本
v 0.1

参考资料：  
下拉刷新  
https://github.com/chrisbanes/Android-PullToRefresh  
http://blog.csdn.net/anobodykey/article/details/9163979  
http://my.oschina.net/tingzi/blog/77711  


