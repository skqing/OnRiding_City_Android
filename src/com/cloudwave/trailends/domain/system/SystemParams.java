package com.cloudwave.trailends.domain.system;

import com.cloudwave.trailends.entity.Entity;
import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Table;

/**
 * @description 系统参数实体类
 * @author DolphinBoy
 * @email dolphinboyo@gmail.com
 * @date 2013-12-9
 * @time 下午9:29:01
 * TODO
 */

@Table(name="te_system_params")
public class SystemParams extends Entity {
	private static final long serialVersionUID = 1L;

	@Column(column="versionName_")
	private String versionName;  //版本名称
	
	@Column(column="versionCode_")
	private Integer versionCode;  //版本号

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public Integer getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(Integer versionCode) {
		this.versionCode = versionCode;
	}

}
