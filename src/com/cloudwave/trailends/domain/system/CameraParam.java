package com.cloudwave.trailends.domain.system;

import com.cloudwave.trailends.entity.Entity;
import com.lidroid.xutils.db.annotation.Table;

/**
 * @description 照相机的参数
 * @author DolphinBoy
 * @email 569141948@qq.com
 * @date 2013-11-29
 * @time 下午9:47:52
 * 
 */

@Table(name="te_camera_param")
public class CameraParam extends Entity {
	private static final long serialVersionUID = 1L;
	
	private int jpegQuality = 60;

	public int getJpegQuality() {
		return jpegQuality;
	}

	public void setJpegQuality(int jpegQuality) {
		this.jpegQuality = jpegQuality;
	}
	
	
}
