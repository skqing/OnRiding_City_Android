package com.cloudwave.trailends.domain;

import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import com.cloudwave.trailends.entity.BaseEntity;
import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Finder;
import com.lidroid.xutils.db.annotation.Table;
import com.lidroid.xutils.db.annotation.Transient;
import com.lidroid.xutils.db.sqlite.FinderLazyLoader;

@Table(name="te_ridingline")
public class RidingLine extends BaseEntity {
	private static final long serialVersionUID = 6572363070793802212L;
	public static final int HASNOT_BEGUN = 0;
	public static final int HAS_BEGUN = 1;
	public static final int HAS_END = 2;
	
	public static final int IS_PUBLIC_NO = 0;
	public static final int IS_PUBLIC_YES = 1;
	
	public RidingLine() {
	}
	public RidingLine(Long id) {
		this.id = id;
	}
	public RidingLine(String title) {
		this.title = title;
	}

	@Column(column="serialNumber_")
	private String serialNumber;
	
	@Column(column="times_")
	private int times;
	
	@Column(column="title_")
	private String title;
	
	@Column(column="startTime_")
	private Date startTime;  //开始时间
	
	@Column(column="endTime_")
	private Date endTime;  //结束时间
	
	@Column(column="description_")
	private String description;  //描述
	
	@Column(column="status_", defaultValue="0")
	private int status = HAS_BEGUN;  //行记的状态
	
	@Column(column="isNow_", defaultValue="0")
	private boolean isNow;  //是否是当前行记
	
	// 此旅程下的所有记录
//	@Finder(valueColumn = "id_", targetColumn = "ridingline_id")
	private List<LinePoint> lpList;
	
	@Finder(valueColumn = "id_", targetColumn = "ridingline_id")
	public FinderLazyLoader<LinePoint> lpLoader;
	
	@Column(column="userWeight_")
	private Float userWeight = DEFAULT_FLOAT_COUNT_NUM;  //用户当时体重,用于计算kcal
	// 统计信息
	@Column(column="totalMileage_")
	private float totalMileage = 0.00f;  //总距离
	@Column(column="totalPoint_")
	private int totalPoint = 0;  //坐标点总数
	@Column(column="totalTime_")
	private long totalTime = 0;  //时长
	@Column(column="totalValidTime_")
	private long totalValidTime = 0;  //有效时长
	
	@Column(column="totalRise_")
	private Float totalRise;  //累计上升
	@Column(column="totalDrop_")
	private Float totalDrop;  //累计下降
//	@Column(column="altitudeRange_")
//	private Float altitudeRange;  //海拔范围
	@Column(column="maxAltitude_")
	private Double maxAltitude;  //最大海拔
	@Column(column="minAltitude_")
	private Double minAltitude;  //最小海拔
	@Column(column="totalClimb_")
	private Float totalClimb; //爬升
	@Column(column="totaDecline_")
	private Float totaDecline;  //下降
	@Column(column="totalUpSlope_")
	private Float totalUpSlope;  //上坡坡程
	@Column(column="totalDownSlope_")
	private Float totalDownSlope;  //下坡陂程
	
	// 统计数据，考虑分离出去
	@Column(column="maxSpeed_", defaultValue="0")
	private Float maxSpeed = DEFAULT_FLOAT_COUNT_NUM;  //最大速度
	private float avgSpeed = DEFAULT_FLOAT_COUNT_NUM;  //平均速度
	@Column(column="maxGpsSpeed_")
	private Float maxGpsSpeed;  //GPS最大速度
	@Column(column="avgGpsSpeed_")
	private Float avgGpsSpeed;  //GPS平均速度
	@Column(column="avgAltitude_")
	private Float avgAltitude = DEFAULT_FLOAT_COUNT_NUM;  //平均海拔
	private Float kcal = DEFAULT_FLOAT_COUNT_NUM;  //消耗卡路里
	
	@Column(column="startAddr_")
	private String startAddr;  //出发位置
	@Column(column="endAddr_")
	private String endAddr;  //结束位置
	
	@Column(column="score_")
	private int score;  //得分
	
	@Column(column="isPublic_", defaultValue="1")
	private int isPublic = IS_PUBLIC_YES;  //行记的状态
	
	@Column(column="isSyncWeibo_", defaultValue="0")
	private int isSyncWeibo;  //是否同步到新浪微博
	
	@Column(column="isSyncWblog_", defaultValue="0")
	private int isSyncWblog;  //是否同步到腾讯微博
	
	// 为什么用String呢，是因为TextView控件的.setText方法不能传如一个整形的值，因为整形只能是资源ID
	@Column(column="commentSum_", defaultValue="0")
	private int commentSum = DEFAULT_COUNT_NUM;  //评论数
	
	@Column(column="zanSum_", defaultValue="0")
	private int zanSum = DEFAULT_COUNT_NUM;  //赞数
	
//	@Column(column="model_")
	private String model;
	
	@Transient
	private boolean hasZan = false;
	@Transient
	private List<User> contributors;  //参与者
	
	public String getTitle() {
		return title;
	}
	public int getTimes() {
		return times;
	}
	public void setTimes(int times) {
		this.times = times;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public List<User> getContributors() {
		return contributors;
	}
	public void setContributors(List<User> contributors) {
		this.contributors = contributors;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getCommentSum() {
		return commentSum;
	}
	public void setCommentSum(int commentSum) {
		this.commentSum = commentSum;
	}
	public int getZanSum() {
		return zanSum;
	}
	public void setZanSum(int zanSum) {
		this.zanSum = zanSum;
	}
	public int getIsPublic() {
		return isPublic;
	}
	public void setIsPublic(int isPublic) {
		this.isPublic = isPublic;
	}
	public boolean hasZan() {
		return hasZan;
	}
	public void hasZan(boolean hasZan) {
		this.hasZan = hasZan;
	}
	public boolean isNow() {
		return isNow;
	}
	public boolean getIsNow() {
		return isNow;
	}
	public void setIsNow(boolean isNow) {
		this.isNow = isNow;
	}
	public int getIsSyncWeibo() {
		return isSyncWeibo;
	}
	public void setIsSyncWeibo(int isSyncWeibo) {
		this.isSyncWeibo = isSyncWeibo;
	}
	public int getIsSyncWblog() {
		return isSyncWblog;
	}
	public void setIsSyncWblog(int isSyncWblog) {
		this.isSyncWblog = isSyncWblog;
	}
	public float getTotalMileage() {
		return totalMileage;
	}
	@SuppressLint("DefaultLocale")
	public String getTotalMileageV() {
		return String.format("%.2f", totalMileage / 1000);
	}
	public void setTotalMileage(float totalMileage) {
		this.totalMileage = totalMileage;
	}
	public int getTotalPoint() {
		return totalPoint;
	}
	public void setTotalPoint(int totalPoint) {
		this.totalPoint = totalPoint;
	}
	public long getTotalTime() {
		return totalTime;
	}
	public void setTotalTime(long totalTime) {
		this.totalTime = totalTime;
	}
	public long getTotalValidTime() {
		return totalValidTime;
	}
	public void setTotalValidTime(long totalValidTime) {
		this.totalValidTime = totalValidTime;
	}
	public List<LinePoint> getLpList() {
		return lpList;
	}
	public void setLpList(List<LinePoint> lpList) {
		this.lpList = lpList;
	}
	public FinderLazyLoader<LinePoint> getLpLoader() {
		return lpLoader;
	}
	public void setLpLoader(FinderLazyLoader<LinePoint> lpLoader) {
		this.lpLoader = lpLoader;
	}
	public Float getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(Float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	
	public float getValidAvgSpeed() {
		if (totalTime > 0) {
			float km = totalMileage / (float) 1000;
			if ( totalValidTime == 0 ) {  //这样做未必妥当###
				totalValidTime = totalTime;
			}
			float hour = (float) totalValidTime / (float) (60 * 60);
			avgSpeed = km / hour;
			return avgSpeed;
		} else {
			return avgSpeed;
		}
	}
	
	public float getAvgSpeed() {
		if (totalTime > 0) {
			float km = totalMileage / (float) 1000;
			float hour = (float) totalTime / (float) (60 * 60);
			avgSpeed = km / hour;
			return avgSpeed;
		} else {
			return avgSpeed;
		}
	}
	
	public void setAvgSpeed(float avgSpeed) {
		this.avgSpeed = avgSpeed;
	}
	/**卡路里(kcal)消耗=骑车时速(km/h)×体重(kg)×9.7×运动时间(h) */
	public Float getKcal() {
		if ( userWeight != null ) {
			float hour = (float) totalTime / (float) (60 * 60);
			kcal = getAvgSpeed() * (float) userWeight * (float) 9.7 * hour;
			return kcal;
		} else {
			return null;
		}
	}
	public void setKcal(Float kcal) {
		this.kcal = kcal;
	}
	public boolean isHasZan() {
		return hasZan;
	}
	public void setHasZan(boolean hasZan) {
		this.hasZan = hasZan;
	}
	public float getAvgAltitude() {
		return avgAltitude;
	}
	public void setAvgAltitude(float avgAltitude) {
		this.avgAltitude = avgAltitude;
	}
	public Float getUserWeight() {
		return userWeight;
	}
	public void setUserWeight(Float userWeight) {
		this.userWeight = userWeight;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Float getTotalRise() {
		return totalRise;
	}
	public void setTotalRise(Float totalRise) {
		this.totalRise = totalRise;
	}
	public Float getTotalDrop() {
		return totalDrop;
	}
	public void setTotalDrop(Float totalDrop) {
		this.totalDrop = totalDrop;
	}
	public String getAltitudeRange() {
		if ( maxAltitude != null && minAltitude != null ) {
			return maxAltitude+"~"+minAltitude;
		} else {
			return "--";
		}
	}
	public Double getMaxAltitude() {
		return maxAltitude;
	}
	public void setMaxAltitude(Double maxAltitude) {
		this.maxAltitude = maxAltitude;
	}
	public Double getMinAltitude() {
		return minAltitude;
	}
	public void setMinAltitude(Double minAltitude) {
		this.minAltitude = minAltitude;
	}
	public Float getTotalClimb() {
		return totalClimb;
	}
	public void setTotalClimb(Float totalClimb) {
		this.totalClimb = totalClimb;
	}
	public Float getTotaDecline() {
		return totaDecline;
	}
	public void setTotaDecline(Float totaDecline) {
		this.totaDecline = totaDecline;
	}
	public Float getTotalUpSlope() {
		return totalUpSlope;
	}
	public void setTotalUpSlope(Float totalUpSlope) {
		this.totalUpSlope = totalUpSlope;
	}
	public Float getTotalDownSlope() {
		return totalDownSlope;
	}
	public void setTotalDownSlope(Float totalDownSlope) {
		this.totalDownSlope = totalDownSlope;
	}
	public Float getMaxGpsSpeed() {
		return maxGpsSpeed;
	}
	public void setMaxGpsSpeed(Float maxGpsSpeed) {
		this.maxGpsSpeed = maxGpsSpeed;
	}
	public Float getAvgGpsSpeed() {
		return avgGpsSpeed;
	}
	public void setAvgGpsSpeed(Float avgGpsSpeed) {
		this.avgGpsSpeed = avgGpsSpeed;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public void setAvgAltitude(Float avgAltitude) {
		this.avgAltitude = avgAltitude;
	}
	public String getStartAddr() {
		return startAddr;
	}
	public void setStartAddr(String startAddr) {
		this.startAddr = startAddr;
	}
	public String getEndAddr() {
		return endAddr;
	}
	public void setEndAddr(String endAddr) {
		this.endAddr = endAddr;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}

}