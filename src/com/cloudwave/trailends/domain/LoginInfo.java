package com.cloudwave.trailends.domain;

import java.util.Date;

import com.cloudwave.trailends.entity.Entity;
import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Foreign;
import com.lidroid.xutils.db.annotation.Table;

/**
 * @description 登录信息 实体类
 * @author DolphinBoy
 * @email dolphinboyo@gmail.com
 * @date 2013-12-29
 * @time 上午10:57:33
 * TODO
 */

@Table(name="app_login_info")
public class LoginInfo extends Entity {
	private static final long serialVersionUID = 1L;
	
	//认证信息
	@Column(column="token_")
	private String token;
	@Column(column="invalidTime_")
	private Date invalidTime;
	
	@Column(column="userId_")
	private Long userId;
	
	@Column(column="loginTime_")
	private Date loginTime;
	
	@Foreign(column = "user_id", foreign = "id_")
	private User user;
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getInvalidTime() {
		return invalidTime;
	}

	public void setInvalidTime(Date invalidTime) {
		this.invalidTime = invalidTime;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

}