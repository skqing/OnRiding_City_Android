package com.cloudwave.trailends.domain;

import java.util.Date;

import com.cloudwave.trailends.entity.BaseEntity;
import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Table;

/**
 * 用户反馈 实体类
 * @author DolphinBoy
 * @date 2014年5月20日
 * TODO
 */

@Table(name="te_feedback")
public class Feedback extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	@Column(column="title_")
	private String title;
	@Column(column="content_")
	private String content;
	@Column(column="qq_")
	private String qq;
	@Column(column="email_")
	private String email;
	@Column(column="mobile_")
	private String mobile;
	@Column(column="model_")
	private String model;
	@Column(column="appVer_")
	private String appVer;
	@Column(column="releaseVer_")
	private String releaseVer;
	@Column(column="remark_")
	private String remark;
	@Column(column="type_")
	private int type;
	@Column(column="level_")
	private int level;
	@Column(column="status_")
	private int status;
	@Column(column="viewCount_")
	private int viewCount;
	@Column(column="responseCount_")
	private int responseCount;
	@Column(column="user_id")
	private long userId;
	@Column(column="updateTime_")
	private Date updateTime;
	@Column(column="createdTime_")
	private Date createdTime;
	
	public Feedback() {
		
	}
	public Feedback(String content) {
		this.content = content;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getAppVer() {
		return appVer;
	}
	public void setAppVer(String appVer) {
		this.appVer = appVer;
	}
	public String getReleaseVer() {
		return releaseVer;
	}
	public void setReleaseVer(String releaseVer) {
		this.releaseVer = releaseVer;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getViewCount() {
		return viewCount;
	}
	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}
	public int getResponseCount() {
		return responseCount;
	}
	public void setResponseCount(int responseCount) {
		this.responseCount = responseCount;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	
}
