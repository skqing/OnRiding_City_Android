package com.cloudwave.trailends.domain;


import java.io.Serializable;
import java.util.Date;

import android.text.TextUtils;

import com.cloudwave.trailends.entity.UserInfoEntity;
import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;
import com.lidroid.xutils.db.annotation.Transient;

/**
 * 
 * @author DolphinBoy
 * User类的ID比较特殊，不继承Entity
 */
@Table(name="app_user")
public class User implements Serializable {
	private static final long serialVersionUID = 1274317527384710184L;
	
	@Id(column="id_")
	@NoAutoIncrement
	private Long id;  //ID
	
	@Column(column="token_")
	private String token;
	
	@Column(column="cacheTime_")
	private Long cacheTime;
	
	@Column(column="account_")
	private String account;
	
	@Column(column="username_")
	private String userName;
	
	@Column(column="nickname_")
	private String nickName;
	
	@Column(column="email_")
	private String email;
	
	@Transient
	private String password;
	
	@Column(column="avatar_")
	private String avatar;
	
	@Column(column="localAvatar_")
	private String localAvatar;
	
	@Column(column="intro_")
	private String intro;
	@Column(column="city_")
	private String city;
	@Column(column="address_")
	private String address;
	
	@Column(column="gender_")
	private int gender;
	@Column(column="age_")
	private Integer age;
	@Column(column="height_")
	private Integer height;
	@Column(column="weight_")
	private Float weight;
	
	@Column(column="totalMileage_")
	private float totalMileage = 0f;
	@Column(column="totalRidingTime_")
	private long totalRidingTime = 0;
	@Column(column="totalRidingLine_")
	private int totalRidingLine = 0;
	@Column(column="totalLinePoint_")
	private long totalLinePoint = 0l;
	
	
	@Column(column="signature_")
	protected String signature;
	@Column(column="introduction_")
	protected String introduction;
	@Column(column="regtime_")
	private Date regtime;
	
	@Column(column="notices_")
	protected int notices;
	@Column(column="replies_")
	protected int replies;
	
	@Column(column="honor_")
	protected int honor;  //荣誉值
	
	@Column(column="favorites_")
	protected int favorites;  //收藏
	@Column(column="follower_")
	protected int follower;  //关注
	
	// 统计数据，考虑用子类来扩展
//	@Column(column="trips_")
//	protected int trips;  //旅程
//	@Column(column="tripRecords_")
//	protected long tripRecords;  //行记
//	@Column(column="mileage_")
//	protected long mileage;  //里程
	
	@Column(column="updateFlag_")
	protected String updateFlag;  //更新标签
	
	@Column(column="isSessionUser_", defaultValue="0")
	protected int isSessionUser = 0;  //是否是会话用户
	
	
	// fastjson反序列化的时候需要一个构造方法
	public User() {
		super();
	}
	
	public User(Long id) {
		super();
		this.id = id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	public void updateBaseInfo(UserInfoEntity userInfo) {
		this.setGender(userInfo.getGender());
		this.setAge(userInfo.getAge());
		this.setHeight(userInfo.getHeight());
		this.setWeight(userInfo.getWeight());
		this.setCity(userInfo.getCity());
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public int getNotices() {
		return notices;
	}

	public void setNotices(int notices) {
		this.notices = notices;
	}

	public int getReplies() {
		return replies;
	}

	public void setReplies(int replies) {
		this.replies = replies;
	}

	public int getHonor() {
		return honor;
	}

	public void setHonor(int honor) {
		this.honor = honor;
	}

	public String getUserName() {
		if ( TextUtils.isEmpty(userName) ) {
			return nickName;
		} else {
			return userName;
		}
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAvatar() {
//		if ( !TextUtils.isEmpty(localAvatar) ) {
//			return localAvatar;
//		} else {
//			return avatar;
//		}
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getAvatarUrl() {
		return this.avatar;
	}
	public String getLocalAvatar() {
		return localAvatar;
	}
	public void setLocalAvatar(String localAvatar) {
		this.localAvatar = localAvatar;
	}
	public String getUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(String updateFlag) {
		this.updateFlag = updateFlag;
	}

	public boolean isSessionUser() {
		return isSessionUser==0?false:true;
	}
	public void isSessionUser(boolean bn) {
		isSessionUser = bn==true?1:0;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public Date getRegtime() {
		return regtime;
	}

	public void setRegtime(Date regtime) {
		this.regtime = regtime;
	}

	public int getIsSessionUser() {
		return isSessionUser;
	}

	public void setIsSessionUser(int isSessionUser) {
		this.isSessionUser = isSessionUser;
	}

	public int getFavorites() {
		return favorites;
	}

	public void setFavorites(int favorites) {
		this.favorites = favorites;
	}

	public int getFollower() {
		return follower;
	}

	public void setFollower(int follower) {
		this.follower = follower;
	}

	public Long getCacheTime() {
		return cacheTime;
	}

	public void setCacheTime(Long cacheTime) {
		this.cacheTime = cacheTime;
	}

	public float getTotalMileage() {
		return totalMileage;
	}

	public void setTotalMileage(float totalMileage) {
		this.totalMileage = totalMileage;
	}

	public int getTotalRidingLine() {
		return totalRidingLine;
	}

	public void setTotalRidingLine(int totalRidingLine) {
		this.totalRidingLine = totalRidingLine;
	}

	public long getTotalLinePoint() {
		return totalLinePoint;
	}

	public void setTotalLinePoint(long totalLinePoint) {
		this.totalLinePoint = totalLinePoint;
	}

	public long getTotalRidingTime() {
		return totalRidingTime;
	}

	public void setTotalRidingTime(long totalRidingTime) {
		this.totalRidingTime = totalRidingTime;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}
	
	
	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

//	public void setGenderText(String gender) {
//		if ( gender != null ) {
//			if ( gender.contains("男") ) {
//				this.gender = 1;
//			} else if ( gender.contains("女") ) {
//				this.gender = 2;
//			} else {
//				this.gender = -1;
//			}
//		}
//	}
//
//	public String getGenderText() {
//		if ( gender == 1 ) {
//			return "男";
//		} else if ( gender == 2 ) {
//			return "女";
//		} else if ( gender == 3 ) {
//			return "其他";
//		} else {
//			return "未知";
//		}
//	}
}
