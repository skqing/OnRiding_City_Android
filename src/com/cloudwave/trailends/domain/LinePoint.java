package com.cloudwave.trailends.domain;



import java.util.Date;

import android.text.TextUtils;

import com.baidu.location.BDLocation;
import com.cloudwave.trailends.entity.BaseEntity;
import com.cloudwave.trailends.utils.DateUtils;
import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Foreign;
import com.lidroid.xutils.db.annotation.Table;

/**
 * 地理位置数据实体
 * @author DolphinBoy
 * @date 2013-7-27
 */

@Table(name="te_linepoint")
public class LinePoint extends BaseEntity {
	private static final long serialVersionUID = -4264803054521125416L;
	public static final int TypeGpsLocation = 1;
	public static final int TypeNetWorkLocation = 2;
	public static final int TypeWIFILocation = 3;
	
	@Column(column="altitude_")
	private Double altitude;  //海拔高度
	@Column(column="accuracy_")
	private Float accuracy;  //定位精度
	@Column(column="direction_")
	private Float direction;  //定位时的方向角度
	@Column(column="latitude_")
	private Double latitude;  //纬度
	@Column(column="longitude_")
	private Double longitude;  //经度
	@Column(column="radius_")
	private Float radius;  //定位精度半径，单位是米
	@Column(column="time_")
	private Long time;  //时间
	@Column(column="speed_")
	private Float speed;  //速度
	
	@Column(column="satellitesNum_")
	private Integer satellitesNum; //定位时卫星的数目
	@Column(column="locType_")
	private int locType;  //定位方式
	
	//反地理编码
//	@Column(column="province_")
//	private String province;  //获取省份信息
//	@Column(column="city_")
//	private String city;  //获取城市信息
//	@Column(column="cityCode_")
//	private String cityCode;  //城市代码
//	@Column(column="district_")
//	private String district;  //获取区县信息
//	private String town;  //城镇,市镇
//	private String street;  //街道
	
	@Column(column="addrStr_")
	private String addrStr;  //地理信息字符串
	@Column(column="poi_")
	private String poi;  //获取poi信息

	//其他信息
	@Column(column="provider_")
	private String provider;
	@Column(column="sdkVer_")
	private String sdkVer;  //SDK版本, 目前使用百度定位SDK
	@Column(column="coorType_")
	private String coorType;  //此坐标采用的坐标系
	
	@Column(column="distance_")
	private Float distance;  //距离(离开始点的距离)
	@Column(column="step_")
	private int step;  //分段
	
	// 这里考虑使用懒加载
//	ForeignLazyLoader<RidingLine> ridingLine
	@Foreign(column="ridingline_id", foreign="id_")
	public RidingLine ridingLine;
	
	/**
	 * 从百度定位SDK中的DBLocation类中解析数据
	 */
	public void parseBDLocation(BDLocation location, String ver, String coorType, int provider) {
		 if (location == null) {
			 return;
		 }
             
//         if (location.getLocType() == BDLocation.TypeGpsLocation) {  //卫星定位 
//             this.setSpeed(location.getSpeed());  
//             this.setSatellitesNum(location.getSatelliteNumber());
//         } else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {  //网络定位 
//        	 this.setAddrStr(location.getAddrStr());//LocationClientOption.setAddrType("all");需要设置  
//        	 this.setProvince(location.getProvince());
//             this.setCity(location.getCity());
//         }
         
		this.setLatitude(location.getLatitude());
        this.setLongitude(location.getLongitude());
        //如果不显示定位精度圈，将accuracy赋值为0即可
        this.setRadius(location.getRadius());
        this.setAltitude(location.getAltitude());
        this.setDirection(location.getDirection());
        
        try {
        	this.setTime(TextUtils.isEmpty(location.getTime())?0:Long.parseLong(location.getTime()));
		} catch (java.lang.NumberFormatException e) {
			Date d = DateUtils.parseDayTime(location.getTime());
			this.setTime(d.getTime());
		}
        
        this.setSpeed(location.getSpeed());
//        this.setDistrict(location.getDistrict());
       
        this.setSpeed(location.getSpeed());  
        this.setSatellitesNum(location.getSatelliteNumber());
        
        this.setSdkVer(ver);
        this.setCoorType(coorType);
        this.setProvider(provider);
	}
	
	public static boolean isValid(LinePoint location) {
		if (location.getLatitude() == null || location.getLongitude() == null ) {
			return false;
		} else {
			return true;
		}
	}
	
	public Double getAltitude() {
		return altitude;
	}
	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}
	public Float getAccuracy() {
		return accuracy;
	}
	public void setAccuracy(Float accuracy) {
		this.accuracy = accuracy;
	}
	public Float getDirection() {
		return direction;
	}
	public void setDirection(Float direction) {
		this.direction = direction;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Float getRadius() {
		return radius;
	}
	public void setRadius(Float radius) {
		this.radius = radius;
	}
	public Float getSpeed() {
		return speed;
	}
	public void setSpeed(Float speed) {
		this.speed = speed;
	}
	public Long getTime() {
		return time;
	}
	public void setTime(Long time) {
		this.time = time;
	}
	public Integer getSatellitesNum() {
		return satellitesNum;
	}
	public void setSatellitesNum(Integer satellitesNum) {
		this.satellitesNum = satellitesNum;
	}
	public int getLocType() {
		return locType;
	}

	public void setLocType(int locType) {
		this.locType = locType;
	}
//	public String getProvince() {
//		return province;
//	}
//	public void setProvince(String province) {
//		this.province = province;
//	}
//	public String getCity() {
//		return city;
//	}
//	public void setCity(String city) {
//		this.city = city;
//	}
//	public String getCityCode() {
//		return cityCode;
//	}
//
//	public void setCityCode(String cityCode) {
//		this.cityCode = cityCode;
//	}
//
//	public String getDistrict() {
//		return district;
//	}
//	public void setDistrict(String district) {
//		this.district = district;
//	}
	public String getPoi() {
		return poi;
	}
	public void setPoi(String poi) {
		this.poi = poi;
	}
	public String getSdkVer() {
		return sdkVer;
	}
	public void setSdkVer(String sdkVer) {
		this.sdkVer = sdkVer;
	}
	public String getAddrStr() {
		return addrStr;
	}
	public void setAddrStr(String addrStr) {
		this.addrStr = addrStr;
	}
	public RidingLine getRidingLine() {
		return ridingLine;
	}
	public void setRidingLine(RidingLine ridingLine) {
		this.ridingLine = ridingLine;
	}

	public String getCoorType() {
		return coorType;
	}

	public void setCoorType(String coorType) {
		this.coorType = coorType;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(int provider) {
		switch (provider) {
		case BDLocation.TypeGpsLocation:
			this.provider = "gps";
			break;
		case BDLocation.TypeNetWorkLocation:
			this.provider = "network";
			break;
			
		default:
			break;
		}
	}

	public Float getDistance() {
		return distance;
	}

	public void setDistance(Float distance) {
		this.distance = distance;
	}

	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}

}
