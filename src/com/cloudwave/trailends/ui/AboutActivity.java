package com.cloudwave.trailends.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.TextView;

import com.cloudwave.trailends.R;
import com.cloudwave.trailends.AppContext;
import com.cloudwave.trailends.compo.BaseActivity;

/**
 * 关于
 * @author DolphinBoy
 * @email dolphinboyo@gmail.com
 * @date 2014-5-11
 * @time 下午3:16:02
 */

public class AboutActivity extends BaseActivity {
	private static final String TAG = "AboutActivity";
	
	private TextView tvVersion;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.about);
		
		initResources();
		intData();
	}

	private void initResources() {
		tvVersion = (TextView) findViewById(R.id.tvVersion);
	}
	
	private void intData() {
		String ver = ((AppContext) getApplication()).getVersionName();
		Log.i(TAG, "ver:"+ver);
		String v = String.format(tvVersion.getText().toString(), ver);
		tvVersion.setText(v);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		AboutActivity.this.finish();
		return super.onTouchEvent(event);
	}
	
}
