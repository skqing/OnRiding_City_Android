package com.cloudwave.trailends.ui;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.cloudwave.trailends.R;
import com.cloudwave.trailends.compo.BaseActivity;

public class DirectionsActivity extends BaseActivity {
//	private static final String TAG = "DirectionsActivity";

	private ImageButton btnBack;
//	private ImageButton btnOK;
	private TextView tvTitle;
	
	private TextView tvContent;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.directions);
		
		initView();
		
		initData();
		
	}


	private void initView() {
		btnBack = (ImageButton) findViewById(R.id.btnBack);
		tvTitle = (TextView) findViewById(R.id.tvTitle);
//		btnOK = (ImageButton) findViewById(R.id.btnOK);
		tvContent = (TextView) findViewById(R.id.tvContent);
		
		tvTitle.setText(R.string.title_directions);
		
		
		btnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				DirectionsActivity.this.finish();
			}
		});
	}
	
	private void initData() {
		StringBuilder html = new StringBuilder();
		
//		String html = "<font color='red'>I love Android</font><br>";
//        html += "<font color='#00ff00'><big><i> I love Android </i></big></font><p>";
//        html += "<big><a href='http://www.baidu.com'>百度</a></big>";
		html.append("1. 定位方式有三种：GPS定位，基站定位，WIFI定位，而本软件记录轨迹时只能使用GPS定位.<br><br>");
		html.append("2. 相比于码表，GPS定位的误差为<font color='#FF0000'>10m</font>，所以路径和里程仅作为参考.<br><br>");
		html.append("3. 不同的手机GPS芯片的质量有好有坏，定位速度也是不同的.<br><br>");
		html.append("4. 只有室外才能接收到GPS信号，室内是没有办法进行GPS定位的，另外空旷的地方比高楼林立的地方信号好.<br><br>");
		html.append("5. GPS和WIFI一样，都比较消耗电量，所以长途请注意携带充电装备.<br><br>");
		html.append("6. 下载离线地图，能节省一大部分流量.<br><br>");
		html.append("7. 请在确保安全的情况下才查看手机.<br><br>");
		html.append("8. 联系方式：<br>");
		html.append("　　8.1 QQ群：344785250<br>");
		html.append("　　8.2  微信：OnRiding");
//		html.append("　　8.3  网站：<a href='http://onriding.cc/forum/flist/2'>车轮不息</a><br>");
		
        CharSequence charSequence = Html.fromHtml(html.toString());
        tvContent.setText(charSequence);
        tvContent.setMovementMethod(LinkMovementMethod.getInstance());
        
	}
	
}
