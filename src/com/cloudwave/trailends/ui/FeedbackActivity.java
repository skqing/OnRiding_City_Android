package com.cloudwave.trailends.ui;

import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.cloudwave.trailends.AppContext;
import com.cloudwave.trailends.Constants;
import com.cloudwave.trailends.R;
import com.cloudwave.trailends.compo.BaseActivity;
import com.cloudwave.trailends.domain.Feedback;
import com.cloudwave.trailends.utils.RegexUtils;
import com.cloudwave.trailends.utils.StatisticsUtils;
import com.cloudwave.trailends.widget.dialog.ColaProgress;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.DbException;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

/**
 * 意见反馈
 * @author DolphinBoy
 * @email dolphinboyo@gmail.com
 * @date 2014-5-11
 * @time 下午2:28:00
 */

public class FeedbackActivity extends BaseActivity {
	private static final String TAG = "FeedbackActivity";
	
	private ImageButton btnBack;
	private TextView tvTitle;
	
	private Button bSend;
	private EditText tvContent;
	private EditText tvContact;
	
	private Feedback feedback = null;
	
	private ColaProgress cp = null;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.feedback);
		
		initView();
		initData();
	}

	private void initView() {
		btnBack = (ImageButton) findViewById(R.id.btnBack);
		tvTitle = (TextView) findViewById(R.id.tvTitle);
//		btnOK = (ImageButton) findViewById(R.id.btnOK);
		bSend = (Button) findViewById(R.id.bSend);
		tvContent = (EditText) findViewById(R.id.tvContent);
		tvContact = (EditText) findViewById(R.id.tvContact);
		
		tvTitle.setText(R.string.title_feedback);
		
		btnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				saveCache();
				FeedbackActivity.this.finish();
			}
		});
		
		bSend.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				initEntity();
				dealSend();
			}
		});
	}
	
	private void initData() {
		String txt = ((AppContext) getApplication()).getAsString(Constants.CACHE_FEEDBACK_TXT);
		if ( !TextUtils.isEmpty(txt) ) {
			tvContent.setText(txt);
		}
	}
	
	private void initEntity() {
		String content = tvContent.getText().toString();
		String contact = tvContact.getText().toString();
		
		feedback = new Feedback(content);
		
		if (!TextUtils.isEmpty(contact)) {
			if (RegexUtils.isMobile(contact)) {
				feedback.setMobile(contact);
			} else if (RegexUtils.isQQ(contact)) {
				feedback.setQq(contact);
			} else if (RegexUtils.isEmail(contact)) {
				feedback.setEmail(contact);
			} else {
				feedback.setRemark(contact);
			}
		}
		
		feedback.setReleaseVer(Build.VERSION.RELEASE);
		feedback.setAppVer(StatisticsUtils.newInstance(this).getVersionName());
		if ( ((AppContext) getApplication()).isLogin() ) {
			feedback.setUserId(((AppContext) getApplication()).getUid());
		}
		feedback.setSendStatus(0);
	}
	
	private void dealSend() {
		cp = ColaProgress.show(FeedbackActivity.this, "提交中...", true, false, null);
		
		RequestParams params = new RequestParams();
		params.addBodyParameter("entity", JSON.toJSONString(feedback));
		
		HttpUtils http = new HttpUtils();
		http.send(HttpMethod.POST, Constants.URL_FEEDBACK_ADD, params, new RequestCallBack<Object>() {

			@Override
			public void onSuccess(ResponseInfo<Object> responseInfo) {
				Toast.makeText(FeedbackActivity.this, "发送成功,感谢反馈^_^", Toast.LENGTH_SHORT).show();
				dealSave(1);
			}

			@Override
			public void onFailure(HttpException error, String msg) {
				Toast.makeText(FeedbackActivity.this, "发送失败,请检查网络~_~", Toast.LENGTH_SHORT).show();
//				dealSave(0);
				cp.dismiss();
			}
		});
	}
	
	private void dealSave(int sendStatus) {
		DbUtils dbu = DbUtils.create(this, Constants.DB_NAME);
		try {
			feedback.setSendStatus(sendStatus);
			dbu.save(feedback);
			if ( sendStatus == 0 ) {
				Toast.makeText(FeedbackActivity.this, "已保存为草稿^_^", Toast.LENGTH_SHORT).show();
			}
			this.finish();
		} catch (DbException e) {
			if ( sendStatus == 0 ) {
				Toast.makeText(FeedbackActivity.this, "保存草稿失败~_~", Toast.LENGTH_SHORT).show();
				cp.dismiss();
			}
			Log.e(TAG, e.getMessage());
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		IntentFilter filter = new IntentFilter();
        filter.addAction("ExitApp");
        this.registerReceiver(this.broadcastReceiver, filter);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		saveCache();
		this.finish();
	}
	
	private void saveCache() {
		String content = tvContent.getText().toString();
		if ( !TextUtils.isEmpty(content) ) {
			((AppContext) getApplication()).put(Constants.CACHE_FEEDBACK_TXT, content);
		}
	}
}
