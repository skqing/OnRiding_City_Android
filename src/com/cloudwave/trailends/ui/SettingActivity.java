package com.cloudwave.trailends.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudwave.trailends.R;
import com.cloudwave.trailends.compo.BaseActivity;
import com.cloudwave.trailends.service.RidingService;
import com.cloudwave.trailends.widget.view.CustomDialog;
import com.umeng.update.UmengUpdateAgent;
import com.umeng.update.UmengUpdateListener;
import com.umeng.update.UpdateResponse;
import com.umeng.update.UpdateStatus;

/**
 * 
 * @author DolphinBoy
 * @email dolphinboyo@gmail.com
 * @date 2014-5-9
 * @time 下午11:36:59
 * TODO
 */

public class SettingActivity extends BaseActivity {
	private static final String TAG = "SettingActivity";
	
	private ImageButton btnBack;
//	private ImageButton btnOK;
	private TextView tvTitle;
	private TextView tvAbout;
	private TextView tvFeedback;
	private TextView tvDirections;
	private TextView tvOfflineMap;
	private TextView tvCheckUpdate;
	private Button bExit;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.setting);
		
		initView();
	}
	
	private void initView() {
		btnBack = (ImageButton) findViewById(R.id.btnBack);
		tvTitle = (TextView) findViewById(R.id.tvTitle);
//		btnOK = (ImageButton) findViewById(R.id.btnOK);
		tvAbout = (TextView) findViewById(R.id.tvAbout);
		tvFeedback = (TextView) findViewById(R.id.tvFeedback);
		tvDirections = (TextView) findViewById(R.id.tvDirections);
		tvOfflineMap = (TextView) findViewById(R.id.tvOfflineMap);
		tvCheckUpdate = (TextView) findViewById(R.id.tvCheckUpdate);
		bExit = (Button) findViewById(R.id.bExit);
		
		tvTitle.setText(R.string.title_setting);
		
//		String ver = ((AppContext) getApplication()).getVersionName();
//		String v = String.format(tvCheckUpdate.getText().toString(), ver);
//		tvCheckUpdate.setText(v);
		
		btnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SettingActivity.this.finish();
			}
		});
		tvAbout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(SettingActivity.this, AboutActivity.class);
				startActivity(intent);
			}
		});
		tvFeedback.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(SettingActivity.this, FeedbackActivity.class);
				startActivity(intent);
			}
		});
		tvDirections.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(SettingActivity.this, DirectionsActivity.class);
				startActivity(intent);
			}
		});
		tvOfflineMap.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(SettingActivity.this, OfflineMapActivity.class);
				startActivity(intent);
			}
		});
		tvCheckUpdate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				update();
			}
		});
		bExit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				exit();
			}
		});
	}
	
	private void update() {
		Log.i(TAG, "检测更新...");
		Toast.makeText(this, "检测中...", Toast.LENGTH_SHORT).show();
		
		UmengUpdateAgent.setUpdateCheckConfig(false);  //禁止集成检测功能
		UmengUpdateAgent.setRichNotification(true);  //设置高级通知栏
		UmengUpdateAgent.setUpdateListener(new UmengUpdateListener() {
		    @Override
		    public void onUpdateReturned(int updateStatus,UpdateResponse updateInfo) {
		        switch (updateStatus) {
		        case UpdateStatus.Yes: // has update
		            UmengUpdateAgent.showUpdateDialog(SettingActivity.this, updateInfo);
		            break;
		        case UpdateStatus.No: // has no update
		            Toast.makeText(SettingActivity.this, "没有更新^_^", Toast.LENGTH_SHORT).show();
		            break;
		        case UpdateStatus.NoneWifi: // none wifi
		            Toast.makeText(SettingActivity.this, "没有WIFI连接， 只在WIFI下更新.", Toast.LENGTH_SHORT).show();
		            break;
		        case UpdateStatus.Timeout: // time out
		            Toast.makeText(SettingActivity.this, "网络连接失败~_~", Toast.LENGTH_SHORT).show();
		            break;
		        }
		    }
		});
		
		UmengUpdateAgent.forceUpdate(SettingActivity.this);
	}
	
	private void exit() {
		// ###退出的时候，如果用户还在骑行过程中，或者用户还在同步数据，是会导致有问题的
		long rlId = RidingService.now_rlId;
		int textId = R.string.dialog_exit;
		if ( rlId != 0 ) {
			textId = R.string.dialog_riding_exit;
		}
		
		CustomDialog dialog = new CustomDialog(SettingActivity.this, R.style.dialog_style, R.layout.dialog
				, textId , new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				switch(v.getId()){
				case R.id.confirm_btn:
					
					exitApp(SettingActivity.this);
					SettingActivity.this.finish();
					
					break;
				default:
		            break;
				}
			}
		});
        dialog.show();
	}
}
