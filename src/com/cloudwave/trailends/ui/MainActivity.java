package com.cloudwave.trailends.ui;

import java.text.DecimalFormat;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudwave.trailends.AppContext;
import com.cloudwave.trailends.Constants;
import com.cloudwave.trailends.R;
import com.cloudwave.trailends.compo.BaseActivity;
import com.cloudwave.trailends.domain.RidingLine;
import com.cloudwave.trailends.service.RidingService;
import com.cloudwave.trailends.utils.DateUtils;
import com.cloudwave.trailends.widget.view.CustomDialog;
import com.cloudwave.trailends.widget.view.ImgBtn;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.exception.DbException;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.media.QQShareContent;
import com.umeng.socialize.media.QZoneShareContent;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.sso.QZoneSsoHandler;
import com.umeng.socialize.sso.RenrenSsoHandler;
import com.umeng.socialize.sso.SinaSsoHandler;
import com.umeng.socialize.sso.SmsHandler;
import com.umeng.socialize.sso.TencentWBSsoHandler;
import com.umeng.socialize.sso.UMQQSsoHandler;
import com.umeng.socialize.sso.UMSsoHandler;
import com.umeng.socialize.weixin.controller.UMWXHandler;
import com.umeng.update.UmengUpdateAgent;
import com.umeng.update.UpdateConfig;

/**
 * @description 
 * @author 龙雪
 * @email 569141948@qq.com
 * @date 2014-4-12
 * @time 下午11:37:20
 * 
 */

public class MainActivity extends BaseActivity {
	private static final String TAG = "MainActivity";
	
	private float totalMileage = 0.00f;  //总距离
	private int distance = 0;  //间距
	private int totalPoint = 0;  //坐标点总数
	private long totalTime = 0;  //时长
	
	private int ridingStatus = 0;  //0:停止, 1:开始, 2:暂停
	private boolean isStop = false;  //是否按了停止按钮
	
    private TextView tvMileage;
    private TextView tvDistance;
    private TextView tvKm;
    private TextView tvlocationStat;
    private ImgBtn tvPoint;
    private ImgBtn tvTime;
    private ImageButton ibStart;
    private ImageButton ibMap;
    private ImageButton ibHistory;
    private ImageButton ibShare;
    private ImageButton ibUser;
    private ImageButton ibSetting;
    
    private RidingReceiver ridingReceiver;
    
    private DecimalFormat df = new DecimalFormat("#0.00");
    
    final UMSocialService Umeng = UMServiceFactory.getUMSocialService("com.umeng.share");
    
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.main_simple);
		
		initView();
//		initData();
		
		boolean isGpsOpen = AppContext.isOPen(this);
		Log.i(TAG, "是否打开了GPS:"+isGpsOpen);
		if ( !isGpsOpen ) {
			AppContext.openGPS(this);
			Toast.makeText(MainActivity.this, "请打开GPS！", Toast.LENGTH_SHORT).show();  //###这里应该弹框
		}
		
		String act = getIntent().getStringExtra("act");
		long rlId = getIntent().getLongExtra("rlId", 0);
		if ( "act_rl_continue".equals(act) && rlId != 0) {  //处理详情页面的继续
			initService(rlId);
		} else {
			initService(null);
		}
		
		UpdateConfig.setDebug( true );  //调试模式
		UmengUpdateAgent.setUpdateCheckConfig( false );  //禁止集成检测功能
//		UmengUpdateAgent.silentUpdate(this);  //静默更新  禁止静默更新，因为用户可能在骑行过程中
		UmengUpdateAgent.setRichNotification( true );  //设置高级通知栏
		UmengUpdateAgent.update( this );  //调用友盟接口检测更新
		
		//友盟统计数据发送策略
//		MobclickAgent.setAutoLocation( true );
		MobclickAgent.setDebugMode( false );
		MobclickAgent.updateOnlineConfig( this );
		
		tvMileage.setText(df.format(totalMileage / 1000));
	}
	
	private void initView() {
		tvMileage = (TextView) findViewById(R.id.tvMileage);
		tvDistance  = (TextView) findViewById(R.id.tvDistance);
		tvKm  = (TextView) findViewById(R.id.tvKm);
		tvlocationStat  = (TextView) findViewById(R.id.tvlocationStat);
		tvPoint = (ImgBtn) findViewById(R.id.tvPoint);
		tvTime = (ImgBtn) findViewById(R.id.tvTime);
		ibStart = (ImageButton) findViewById(R.id.ibStart);
//		ibStop = (ImageButton) findViewById(R.id.ibStop);
		ibMap = (ImageButton) findViewById(R.id.ibMap);
		ibHistory = (ImageButton) findViewById(R.id.ibHistory);
		ibShare = (ImageButton) findViewById(R.id.ibShare);
		ibUser = (ImageButton) findViewById(R.id.ibUser);
		ibSetting = (ImageButton) findViewById(R.id.ibSetting);
		
		tvMileage.setText("0.00");
		String tInterStr = getResources().getString(R.string.mark_add);  
        String tNewInterStr = String.format(tInterStr, 0);  
        tvDistance.setText(tNewInterStr);
		
		tvPoint.setTextViewText(String.valueOf(totalPoint));
		tvPoint.setImageResource(R.drawable.stat_point);
		String tStr = getResources().getString(R.string.stat_times);  
        String tNewStr = String.format(tStr, 0, 0, 0);  
        tvTime.setTextViewText(String.valueOf(tNewStr));
        tvTime.setImageResource(R.drawable.stat_time);
        
		ibStart.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				start();
			}
		});
		
		ibMap.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, MapViewActivity.class);
				intent.putExtra("ridingStatus", ridingStatus);
				startActivity(intent);
			}
		});
		
		ibShare.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {
		    	share();
		    }
		});
		ibUser.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {
		    	openOrLogin();
		    }
		});
		ibHistory.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, HistoryActivity.class);
				startActivity(intent);
			}
		});
		ibSetting.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, SettingActivity.class);
				startActivity(intent);
			}
		});
		
//		GeoPoint p1LL = new GeoPoint((int) (22.525164*1e6), (int) (113.92006*1e6));
//		GeoPoint p2LL = new GeoPoint((int) (22.525139*1e6), (int) (113.91573*1e6));
		
//		GeoPoint p1LL = new GeoPoint((int) (22.528998*1e6), (int) (113.922405*1e6));
//		GeoPoint p2LL = new GeoPoint((int) (22.528985*1e6), (int) (113.922396*1e6));
//		double distance = DistanceUtil.getDistance(p1LL, p2LL);
//		Log.i(TAG, "distance:"+distance);
//		Toast.makeText(MainActivity.this, "distance:"+distance, Toast.LENGTH_LONG).show();
	}
	
	private void start() {
		if (ridingStatus == 0) {
			boolean isGpsOpen = AppContext.isOPen(MainActivity.this);
			if ( !isGpsOpen ) {
//				AppContext.openGPS(MainActivity.this);
				openGps();
//				Toast.makeText(MainActivity.this, "请打开GPS！", Toast.LENGTH_SHORT).show();  //###这里应该弹框
			}
			
			
			Intent intent = new Intent();
 			intent.setAction(RidingService.CTRL_START);
 			sendBroadcast(intent);
			
			ridingStatus = 1;
			isStop = false;
			
//			ibStart.setImageResource(R.drawable.myeffect_stop);
//			ibStart.setBackgroundResource(R.drawable.img_background_letter_pause);
			ibStart.setImageResource(R.drawable.ctrl_stop_pause);
		} else if (ridingStatus == 1) {
			
			dialogStopRiding();
			
		} 
//		else if (ridingStatus == 2) {
//			Intent intent = new Intent();
// 			intent.setAction(RidingService.CTRL_CONTINUE);
// 			long id = getIntent().getLongExtra("rlId", 0);
// 			intent.putExtra("rlId", id);
// 			sendBroadcast(intent);
// 			
// 			ridingStatus = 1;
// 			
//// 			ibStart.setImageResource(R.drawable.myeffect_stop);
////			ibStart.setBackgroundResource(R.drawable.img_background_letter_pause);
//			ibStart.setImageResource(R.drawable.ctrl_stop_pause);
//		}
	}
	
	private void openGps() {
		Intent intent = new Intent();
        intent.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            this.startActivity(intent);
        } catch(ActivityNotFoundException ex) {
            // The Android SDK doc says that the location settings activity
            // may not be found. In that case show the general settings.
            
            // General settings activity
            intent.setAction(Settings.ACTION_SETTINGS);
            try {
            	this.startActivity(intent);
            } catch (Exception e) {
            	Log.e(TAG, "打开系统设置异常:", e);
            }
        }
	}
	
	private void dialogStopRiding() {
		CustomDialog dialog = new CustomDialog(MainActivity.this, R.style.dialog_style
				, R.layout.dialog, R.string.dialog_stop_riding, new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				switch(v.getId()){
				case R.id.confirm_btn:

					stopRidingLine();
					
					break;
				default:
		            break;
				}
			}
		});
        dialog.show();
	}
	
	private void stopRidingLine() {
		Intent intent = new Intent();
			intent.setAction(RidingService.CTRL_STOP);
			sendBroadcast(intent);
			
		ridingStatus = 0;
		isStop = true;
		
//		ibStart.setImageResource(R.drawable.myeffect_start);
//		ibStart.setBackgroundResource(R.drawable.img_background_letter);
		ibStart.setImageResource(R.drawable.ctrl_start);
		
		reset();
		//停止后预览或者打开列表
	}
	
	private void reset() {
		totalMileage = 0.00f;
		totalPoint = 0;
		totalTime = 0l;
		distance = 0;
		
		tvMileage.setText("0.00");
		String tInterStr = getResources().getString(R.string.mark_add);  
        String tNewInterStr = String.format(tInterStr, distance); 
        tvDistance.setText(tNewInterStr);
		tvPoint.setTextViewText(String.valueOf(totalPoint));
		String tStr = getResources().getString(R.string.stat_times);  
        String tNewStr = String.format(tStr, 0, 0, 0);  
		tvTime.setTextViewText(String.valueOf(tNewStr));
	}
	
	private void initService(Long rlId) {
		String className = "com.cloudwave.trailends.service";
		if ( !AppContext.isServiceRunning(MainActivity.this, className) ) {  //这个方法判断的不正确###
			Log.i(TAG, "["+className+"]服务没有运行!");
			Intent intent = new Intent(MainActivity.this, RidingService.class);
			intent.setAction(RidingService.RIDING_SERVICE);
			startService(intent);
		}
		
		if (RidingService.now_rlId == 0 && rlId != null) {
			DbUtils dbu = DbUtils.create(this, Constants.DB_NAME);
			RidingLine rl = null;
			try {
				rl = dbu.findById(RidingLine.class, rlId);
			} catch (DbException e) {
				Toast.makeText(MainActivity.this, "加载骑记失败~_~", Toast.LENGTH_SHORT).show();
				Log.w(TAG, "查询记录数据异常:", e);
//				this.finish() ;
				return ;
			}
			
			if ( rl == null ) {
				Toast.makeText(MainActivity.this, "骑记不存在或已经删除~_~", Toast.LENGTH_SHORT).show();
//				this.finish() ;
				return ;
			} else {
//				RidingService.now_rlId = rl.getId();
				totalMileage = rl.getTotalMileage();
				totalPoint = rl.getTotalPoint();
				totalTime = rl.getTotalTime();
				
				if (totalMileage > 0) {
					tvMileage.setText(df.format(totalMileage / 1000));
				} else {
					tvMileage.setText(String.valueOf("0.00"));
				}
				tvPoint.setTextViewText(String.valueOf(totalPoint));
				String tStr = getResources().getString(R.string.stat_times);
				int[] time = DateUtils.duration(totalTime * 1000);
		        String tNewStr = String.format(tStr, time[1], time[2], time[3]);
				tvTime.setTextViewText(String.valueOf(tNewStr));
				
//						ibStart.setImageResource(R.drawable.myeffect_continue);
//						ibStart.setBackgroundResource(R.drawable.img_background_letter);
				ibStart.setImageResource(R.drawable.ctrl_restart);
				
				ridingStatus = 2;
			}
		}
		
	}
	
	public class RidingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.i(TAG, "---收到通知---");
			if ( isStop ) {  //如果是用户手动按了停止按钮，则不再接受通知
				return ;
			}
			String action = intent.getAction();
			if (action.equals(RidingService.UPDATE_STAT)) {
				Log.i(TAG, "---UPDATE_STAT.通知---");
				ridingStatus = 1;
				totalMileage = intent.getFloatExtra("totalMileage", 0.00f);
				totalPoint = intent.getIntExtra("totalPoint", 0);
				totalTime = intent.getLongExtra("totalTime", 0l);
				distance = intent.getIntExtra("distance", 0);
				
				Log.i(TAG, "---totalMileage---"+totalMileage);
				
				if (totalMileage > 0) {
					tvMileage.setText(df.format(totalMileage / 1000));
				} else {
					tvMileage.setText(String.valueOf("0.00"));
				}
				// 随着totalMileage的值的大小动态改变字体大小
				if (totalMileage > 100 * 1000) {
					tvMileage.setTextSize(TypedValue.COMPLEX_UNIT_SP, 42);
					tvDistance.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
					tvKm.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
				} else if (totalMileage > 1000 * 1000) {
					tvMileage.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
					tvDistance.setTextSize(TypedValue.COMPLEX_UNIT_SP, 5);
					tvKm.setTextSize(TypedValue.COMPLEX_UNIT_SP, 5);
				}
				
				String tInterStr = getResources().getString(R.string.mark_add);  
		        String tNewInterStr = String.format(tInterStr, distance); 
		        tvDistance.setText(tNewInterStr);
				tvPoint.setTextViewText(String.valueOf(totalPoint));
				String tStr = getResources().getString(R.string.stat_times);
				int[] time = DateUtils.duration(totalTime * 1000);
		        String tNewStr = String.format(tStr, time[1], time[2], time[3]);
				tvTime.setTextViewText(tNewStr);
				
				
//				ibStart.setImageResource(R.drawable.myeffect_stop);
				if (distance == 0) {
					ibStart.setImageResource(R.drawable.ctrl_stop_pause);
//					ibStart.setBackgroundResource(R.drawable.img_background_letter_pause);
				} else {
					ibStart.setImageResource(R.drawable.ctrl_stop_run);
//					ibStart.setBackgroundResource(R.drawable.img_background_letter);
				}
			} else if (action.equals(RidingService.UPDATE_PAUSE)) {  //后台服务判断两点之间的距离没有超过合理距离则发此通知
				ridingStatus = 0;
				distance = intent.getIntExtra("distance", 0);
				
				String tInterStr = getResources().getString(R.string.mark_add);  
		        String tNewInterStr = String.format(tInterStr, distance); 
		        tvDistance.setText(tNewInterStr);
		        
		        if (distance == 0) {
		        	ibStart.setImageResource(R.drawable.ctrl_stop_pause);
		        }
			} else if (action.equals(RidingService.LOCATION_STAT)) {
				int locationStat = intent.getIntExtra("locationStat", 0);
				
				if ( locationStat == 1 ) {
					tvlocationStat.setVisibility(View.VISIBLE);
					tvlocationStat.setTextColor(android.graphics.Color.BLUE);
					tvlocationStat.setText("定位中...");
				} else if ( locationStat == 2 ) {
					
				}
			} else if (action.equals(RidingService.CTRL_STOP)) {
				//###从详情页点击“结束”按钮应该停止当前骑记，但是这里不起作用
				//应为在onStop取消了广播注册，从详情页返回到列表页，再返回到主页面时调用onStart事件
				//候重新注册了广播，但已经接受不到来自详情页的广播了
				ridingStatus = 0;
				isStop = true;
				
//				ibStart.setImageResource(R.drawable.myeffect_start);
//				ibStart.setBackgroundResource(R.drawable.img_background_letter);
				ibStart.setImageResource(R.drawable.ctrl_start);
				
				reset();
			}
		}
	}
	
	@Override 
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    /**使用SSO授权必须添加如下代码 */
	    UMSsoHandler ssoHandler = Umeng.getConfig().getSsoHandler(requestCode) ;
	    if (ssoHandler != null) {
	       ssoHandler.authorizeCallBack(requestCode, resultCode, data);
	    }
	}
	
	private void openOrLogin() {
		if ( !((AppContext) getApplication()).isLogin() ) {
			Intent intent = new Intent(MainActivity.this, SigninActivity.class);
			intent.putExtra("page", 1);
			startActivity(intent);
		} else {
			Intent intent = new Intent(MainActivity.this, UserActivity.class);
			startActivity(intent);
		}
	}
	
	@Override
	protected void onStart() {
		Log.i(TAG, "--onStart--");
		super.onStart();
		registerReceiver();
		
		String act = getIntent().getStringExtra("act");
		long rlId = getIntent().getLongExtra("rlId", 0);
		if ( !"act_rl_continue".equals(act) && rlId == 0
				 && RidingService.now_rlId == 0 && ridingStatus != 0 ) {
			ridingStatus = 0;
			isStop = true;
			
//				ibStart.setImageResource(R.drawable.myeffect_start);
//				ibStart.setBackgroundResource(R.drawable.img_background_letter);
			ibStart.setImageResource(R.drawable.ctrl_start);
			
			reset();
		}
		
	}

	private void registerReceiver() {
		ridingReceiver = new RidingReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(RidingService.UPDATE_STAT);
		filter.addAction(RidingService.UPDATE_PAUSE);
		filter.addAction(RidingService.CTRL_STOP);
		registerReceiver(ridingReceiver, filter);
	}
	
	@Override
	protected void onStop() {
		Log.i(TAG, "--onStop--");
		if (ridingReceiver != null) {
			this.unregisterReceiver(ridingReceiver);
		}
		super.onStop();
	}
	
	private void share() {
		Umeng.getConfig().setPlatforms(SHARE_MEDIA.QQ, SHARE_MEDIA.QZONE, SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE
		, SHARE_MEDIA.SINA, SHARE_MEDIA.TENCENT, SHARE_MEDIA.RENREN, SHARE_MEDIA.SMS);
		
		Umeng.setShareContent("车轮不息是一个专注于骑行的APP，主要面向城市骑行爱好者。具有定位, 轨迹记录, 数据同步等功能。简单、实用、优雅，欢迎骑友试用!"+Constants.SHARE_TARGET_URL);
		Umeng.setAppWebSite(SHARE_MEDIA.RENREN, Constants.SHARE_TARGET_URL);
//		Umeng.setAppWebSite("http://onriding.cc/appinfo");
		Umeng.setAppWebSite(Constants.SHARE_TARGET_URL);
		Umeng.setShareImage(new UMImage(this, R.drawable.ic_launcher));
		Umeng.setShareMedia(new UMImage(this, R.drawable.ic_launcher));
		
		QQShareContent qqShareContent = new QQShareContent(); //设置分享文字
		qqShareContent.setShareContent("车轮不息是一个专注于骑行的APP，主要面向城市骑行爱好者。具有定位, 轨迹记录, 数据同步等功能。简单、实用、优雅，欢迎骑友试用!");
		qqShareContent.setTitle("车轮不息，专注于骑行！"); //设置分享title
		qqShareContent.setShareImage(new UMImage(this, R.drawable.ic_launcher)); //设置分享图片
		qqShareContent.setTargetUrl(Constants.SHARE_TARGET_URL); //设置点击分享内容的跳转链接
		Umeng.setShareMedia(qqShareContent);
		
		QZoneShareContent qzone = new QZoneShareContent();
		qzone.setShareContent("车轮不息是一个专注于骑行的APP，主要面向城市骑行爱好者。具有定位, 轨迹记录, 数据同步等功能。简单、实用、优雅，欢迎骑友试用!");  //设置分享文字
		qzone.setTitle("车轮不息，专注于骑行！");  //设置分享内容的标题
//		qzone.setShareImage(new UMImage(this, "http://onriding.cc/favicon_64.ico"));  //设置分享图片, 服务器上要放一个圆角的
		qzone.setShareImage(new UMImage(this, R.drawable.ic_launcher));  //设置分享图片
//		qzone.setTargetUrl("http://onriding.cc/appinfo");  //设置点击消息的跳转URL
		qzone.setTargetUrl(Constants.SHARE_TARGET_URL);  //设置点击消息的跳转URL,前期直接写APP下载地址
		Umeng.setShareMedia(qzone);
		
		
		//QQSSO 参数1为当前Activity， 参数2为开发者在QQ互联申请的APP ID，参数3为开发者在QQ互联申请的APP kEY.
		UMQQSsoHandler qqSsoHandler = new UMQQSsoHandler(this, Constants.SECURITY_QQ_APP_ID,
				Constants.SECURITY_QQ_APP_SECRET);
		qqSsoHandler.addToSocialSDK(); 
				
		//QQ空间SSO 参数1为当前Activity，参数2为开发者在QQ互联申请的APP ID，参数3为开发者在QQ互联申请的APP kEY.
		QZoneSsoHandler qZoneSsoHandler = new QZoneSsoHandler(this, Constants.SECURITY_QQ_APP_ID,
				Constants.SECURITY_QQ_APP_SECRET);
		qZoneSsoHandler.addToSocialSDK();
		
		SmsHandler smsHandler = new SmsHandler();
		smsHandler.addToSocialSDK();
		
//		// 添加微信平台
		UMWXHandler wxHandler = new UMWXHandler(this, Constants.SECURITY_WEIXIN_APP_ID, Constants.SECURITY_WEIXIN_APP_SECRET);
		wxHandler.showCompressToast(false);  //解决图片大小超过32k的BUG
		wxHandler.addToSocialSDK();
		
		// 添加微信朋友圈
		UMWXHandler wxCircleHandler = new UMWXHandler(this, Constants.SECURITY_WEIXIN_APP_ID, Constants.SECURITY_WEIXIN_APP_SECRET);
		wxCircleHandler.showCompressToast(false);
		wxCircleHandler.setToCircle(true);
		wxCircleHandler.addToSocialSDK();
		
		
		//设置新浪SSO handler
		Umeng.getConfig().setSsoHandler(new SinaSsoHandler());
		
		//设置腾讯微博SSO handler
		Umeng.getConfig().setSsoHandler(new TencentWBSsoHandler());
		
		RenrenSsoHandler renrenSsoHandler = new RenrenSsoHandler(this,
				Constants.SECURITY_RENREN_APP_ID, Constants.SECURITY_RENREN_KEY,
				Constants.SECURITY_RENREN_KEY_SECRET);
		Umeng.getConfig().setSsoHandler(renrenSsoHandler);
		
		
//		smsHandler.setTargetUrl("http://shouji.360tpcdn.com/140922/c3d6357c65427b55627111114da023a7/com.cloudwave.trailends_10.apk");
//		Umeng.getConfig().setSsoHandler(smsHandler);
		
//		ICallbackListener shareListener = new SnsPostListener() {
//			@Override
//			public void onStart() {
//				Log.i(TAG, "onStart");
//			}
//			@Override
//			public void onComplete(SHARE_MEDIA platform, int eCode, SocializeEntity entity) {
//				Toast.makeText(MainActivity.this, "分享成功", Toast.LENGTH_SHORT).show();
//				Log.i(TAG, "onComplete-->分享平台："+platform);
//				UMedia um = UMedia.SINA_WEIBO;
//				if (platform.toString().equalsIgnoreCase(UMedia.TENCENT_QZONE.toString())) {
//					um = UMedia.TENCENT_QZONE;
//				} else if (platform.toString().equalsIgnoreCase(UMedia.TENCENT_WEIBO.toString())) {
//					um = UMedia.TENCENT_WEIBO;
//				} else if (platform.toString().equalsIgnoreCase(UMedia.RENREN.toString())) {
//					um = UMedia.RENREN;
//				} else if (platform.toString().equalsIgnoreCase(UMedia.DOUBAN.toString())) {
//					um = UMedia.DOUBAN;
//				}
//				
//				//###不知道怎么搞社交分享统计
//				UMPlatformData pfData = new UMPlatformData(um, entity.mEntityKey);
//				pfData.setWeiboId(entity.mCustomID);  //optional
//				MobclickAgent.onSocialEvent(MainActivity.this, pfData);
//				
//			}
//		};
		
//		Umeng.registerListener(shareListener);
//		mController.getConfig().cleanListeners();
		// 是否只有已登录用户才能打开分享选择页
		Umeng.openShare(this, false);
	}

	@Override
	protected void onPause() {
//		Umeng.unregisterListener(shareListener);
		super.onPause();
	}
	
	
	//监听home键
//	private BroadcastReceiver mHomeKeyEventReceiver = new BroadcastReceiver() {
//		String SYSTEM_REASON = "reason";
//		String SYSTEM_HOME_KEY = "homekey";
//		String SYSTEM_HOME_KEY_LONG = "recentapps";
//		 
//		@Override
//		public void onReceive(Context context, Intent intent) {
//			String action = intent.getAction();
//			if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
//				String reason = intent.getStringExtra(SYSTEM_REASON);
//				if (SYSTEM_HOME_KEY.equals(reason)) {
//					 //表示按了home键,程序到了后台
//					if ( isTop ) {
//						MainActivity.this.finish();
//					}
//				} else if (SYSTEM_HOME_KEY_LONG.equals(reason)) {
//					//表示长按home键,显示最近使用的程序列表
//				}
//			} 
//		}
//	};
	
//	public boolean isTopActivy(String mPackageName) {
//        ActivityManager manager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
//        List<RunningTaskInfo> tasksInfo = manager.getRunningTasks(1);
//        if (tasksInfo.size() > 0) {
//        	Log.d(TAG, tasksInfo.get(0).topActivity.getClassName());
//            if (mPackageName.equals(tasksInfo.get(0).topActivity.getShortClassName())) {
//                return true;
//            }
//        }
//        return false;
//	}
}
