package com.cloudwave.trailends.enums;

/**
 * @author DolphinBoy
 * 2013年10月26日 上午10:13:42
 * TODO
 */

public enum LocateStatus {
	LOCATE,  //定位
	COMPASS,  //罗盘
	FOLLOW  //跟随
}
