package com.cloudwave.trailends.entity;

import java.io.Serializable;
import java.util.Map;

/**
 * 获取到SSO信息之后提交到服务器的实体
 * @author 龙雪
 *
 */
public class SsoEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String platform;
	private Integer gender;
	private String nickname;
	private String openid;
	private String avatar;
	private String token;
	
	private String city;
	private String description;
	private String ext1;
	private String ext2;
	private String ext3;
	private String ext4;
	private String ext5;
	
	
	
	/** 微博登录返回数据--龙心安兰
	 {
	    uid=1680947442,
	    favourites_count=187,
	    location=广东深圳,
	    description=Growoldalongwithme,
	    thebestisyettobe.,
	    verified=false,
	    friends_count=267,
	    gender=1,
	    screen_name=龙心安兰,
	    statuses_count=1156,
	    followers_count=147,
	    profile_image_url=http: //tp3.sinaimg.cn/1680947442/180/40006191966/1,
	    access_token=2.00WNFlpB4RG1eD332ef3f7b2sX62kC
	}
	 */
	
	/** QQ登录返回数据 doOauthVerify->Bundle value
	 {
	    access_token=2C39E11BB67A9B80684AFAAC8207874F,
	    openid=33135BE7B50C4F00B3C9019F119D059D,
	    expires_in=7776000,
	    pay_token=766F3E0426AD638E1EAD585A0F7B8E4D,
	    pf=desktop_m_qq-10000144-android-2002-,
	    ret=0,
	    uid=33135BE7B50C4F00B3C9019F119D059D,
	    sendinstall=,
	    appid=,
	    pfkey=b305db12a8ae6734e8bcd04d26093d05,
	    page_type=,
	    auth_time=
	}
	
	getPlatformInfo->Map<String, Object> info
	{
	    is_yellow_year_vip=0,
	    vip=0,
	    level=0,
	    province=广东,
	    yellow_vip_level=0,
	    is_yellow_vip=0,
	    gender=男,
	    screen_name=沁园春···雪,
	    msg=,
	    profile_image_url=http: //q.qlogo.cn/qqapp/101106052/33135BE7B50C4F00B3C9019F119D059D/100,
	    city=深圳
	}
	 */
	
	
	public SsoEntity() {
		super();
	}
	
	public SsoEntity(String platform, Map<String, Object> info) {
		super();
		this.platform = platform;
		
		if ( "qq".equalsIgnoreCase(platform) ) {
			openid = info.get("uid").toString();
			if ( info.get("gender") != null ) {
				if ( "男".equals(info.get("gender").toString()) ) {
					gender = 1;
				} else if ( "女".equals(info.get("gender").toString()) ) {
					gender = 2;
				} else {
					gender = 3;
				}
			}
			
			nickname = info.get("screen_name").toString();
			openid = info.get("openid").toString();
			avatar = info.get("profile_image_url")==null?"":info.get("profile_image_url").toString();
			token = info.get("access_token").toString();
			city = info.get("city")==null?"":info.get("city").toString();
//			ext1 = info.get("verified")==null?"":info.get("verified").toString();
		} else if ( "sina".equalsIgnoreCase(platform) ) {
			if ( info.get("gender") != null ) {
				gender = Integer.parseInt(info.get("gender").toString());
			}
			nickname = info.get("screen_name").toString();
			openid = "weibo_"+info.get("uid").toString();  //新浪微博没有返回openid用"sina"+uid作为openid
			avatar = info.get("profile_image_url")==null?"":info.get("profile_image_url").toString();
			token = info.get("access_token").toString();
			city = info.get("location")==null?"":info.get("location").toString();
			description = info.get("description")==null?"":info.get("description").toString();
			ext1 = info.get("verified")==null?"":info.get("verified").toString();
			ext2 = info.get("favourites_count")==null?"":info.get("favourites_count").toString();
			ext3 = info.get("friends_count")==null?"":info.get("friends_count").toString();
			ext4 = info.get("statuses_count")==null?"":info.get("statuses_count").toString();
			ext5 = info.get("followers_count")==null?"":info.get("followers_count").toString();
		} else if ( "weixin".equalsIgnoreCase(platform) ) {
			
		}
		
		
	}
	

	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public Integer getGender() {
		return gender;
	}
	public void setGender(Integer gender) {
		this.gender = gender;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getExt1() {
		return ext1;
	}
	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}
	public String getExt2() {
		return ext2;
	}
	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}
	public String getExt3() {
		return ext3;
	}
	public void setExt3(String ext3) {
		this.ext3 = ext3;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getExt4() {
		return ext4;
	}
	public void setExt4(String ext4) {
		this.ext4 = ext4;
	}
	public String getExt5() {
		return ext5;
	}
	public void setExt5(String ext5) {
		this.ext5 = ext5;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
