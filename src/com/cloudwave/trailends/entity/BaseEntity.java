package com.cloudwave.trailends.entity;

import java.util.Date;

import com.cloudwave.trailends.domain.User;
import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Foreign;
import com.lidroid.xutils.db.annotation.Transient;

public abstract class BaseEntity extends Entity {
	
	private static final long serialVersionUID = 2540806303084165777L;

	public static final int SENT_FAILED = 0;
	public static final int SENT_SUCCESS = 1;
	
	public static final int DEFAULT_COUNT_NUM = 0;
	
	public static final float DEFAULT_FLOAT_COUNT_NUM = 0.0f;
	
	
	// 数据更新状态
	public static final int CRUD_STATUS_NONE = 0;  //无需更新
	public static final int CRUD_STATUS_ADD = 1;  //新增
	public static final int CRUD_STATUS_UPDATE = 2;  //更新
	public static final int CRUD_STATUS_DELETE = 3;  //删除
	
	
	@Column(column="remoteId_")
	protected Long remoteId;  //服务器端的ID
	
	@Column(column="sendTime_")
	protected Date sendTime;  //发送时间
	
	@Column(column="sendStatus_", defaultValue="0")
	protected int sendStatus = 0;  //发送状态:默认为未发送,或者发送失败
	
	@Column(column="crudStatus_", defaultValue="0")
	protected int crudStatus = CRUD_STATUS_NONE;  //数据更新状态
	
	@Column(column="createTime_")
	protected Date createTime = new Date();  //数据创建时间
	
	@Column(column="timestamp_")
	protected Long timestamp;  //数据写入时间戳
	
	
//	@Column(column="lastUpdateTime_")
//	protected Date lastUpdateTime = new Date();  //数据最后修改时间
	
	// 这里考虑使用懒加载
	@Foreign(column="creator_id", foreign="id_")
	protected User creator;  //创建者
	@Column(column="creator_id")
	protected Long creatorId;  //发表人ID
	
	@Column(column="ext1_")
	protected String ext1;
	@Column(column="ext2_")
	protected String ext2;
	@Column(column="ext3_")
	protected String ext3;
	@Column(column="ext4_")
	protected String ext4;
	@Column(column="ext5_")
	protected String ext5;
	
	
	@Transient
	protected String userName;  //发表人昵称
	@Transient
	protected String userIcon;  //发表人头像
	
	
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	public int getSendStatus() {
		return sendStatus;
	}
	public void setSendStatus(int sendStatus) {
		this.sendStatus = sendStatus;
	}
	public int getCrudStatus() {
		return crudStatus;
	}
	public void setCrudStatus(int crudStatus) {
		this.crudStatus = crudStatus;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public Long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
	//	public Date getLastUpdateTime() {
//		return lastUpdateTime;
//	}
//	public void setLastUpdateTime(Date lastUpdateTime) {
//		this.lastUpdateTime = lastUpdateTime;
//	}
	public User getCreator() {
		return creator;
	}
	public void setCreator(User creator) {
		if (creator != null) {
			this.creatorId = creator.getId();
		}
		this.creator = creator;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserIcon() {
		return userIcon;
	}
	public void setUserIcon(String userIcon) {
		this.userIcon = userIcon;
	}
	public Long getRemoteId() {
		return remoteId;
	}
	public void setRemoteId(Long remoteId) {
		this.remoteId = remoteId;
	}
	public Long getCreatorId() {
		if (getCreator() != null) {
			return getCreator().getId();
		} else {
			return creatorId;
		}
	}
	public void setCreatorId(Long creatorId) {
		if (creator == null) {
			creator = new User(creatorId);
		}
		this.creatorId = creatorId;
	}
	
	public String getExt1() {
		return ext1;
	}
	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}
	public String getExt2() {
		return ext2;
	}
	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}
	public String getExt3() {
		return ext3;
	}
	public void setExt3(String ext3) {
		this.ext3 = ext3;
	}
	public String getExt4() {
		return ext4;
	}
	public void setExt4(String ext4) {
		this.ext4 = ext4;
	}
	public String getExt5() {
		return ext5;
	}
	public void setExt5(String ext5) {
		this.ext5 = ext5;
	}
}
