package com.cloudwave.trailends.entity;

import java.io.Serializable;

public class ResultEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	public int result;
	public int code;
	public Object data;
	
	public boolean isSuccess() {
		if (result == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean hasData() {
		if ( data != null ) {
			return true;
		} else {
			return true;
		}
	}
	
	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getCodeMsg() {
		switch (this.code) {
		case 0:
			return "操作失败!";
		case 112:
			return "非法用户名!";
		case 113:
			return "非法密码!";
		case 114:
			return "非法邮箱!";
		case 124:
			return "用户不存在!";
		case 134:
			return "还未授权,请重新授权!";	
		case 125:
			return "用户名错误!";
		case 126:
			return "密码错误!";
		case 205:
			return "解析错误!";
		case 206:
			return "非法数据!";	
			
		case 302:
			return "查询数据异常!";
		case 303:
			return "更新数据异常!";
		case 401:
			return "数据丢失或已经被删除!";
		default:
			return "未知异常!";
		}
	}

	@Override
	public String toString() {
		return "ResultEntity [result=" + result + ", code=" + code + ", data="
				+ data + "]";
	}
}
