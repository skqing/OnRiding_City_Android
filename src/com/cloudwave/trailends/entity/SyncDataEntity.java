package com.cloudwave.trailends.entity;

import java.io.Serializable;
import java.util.List;

import com.cloudwave.trailends.domain.LinePoint;
import com.cloudwave.trailends.domain.RidingLine;

public class SyncDataEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private long totalSize;  //剩余的总数据量
	private int pageSize;  //每页固定数据量
	private RidingLine entity;  //骑记
	private List<LinePoint> subEntityList;  //坐标点
	
	public long getTotalSize() {
		return totalSize;
	}
	public void setTotalSize(long totalSize) {
		this.totalSize = totalSize;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public RidingLine getEntity() {
		return entity;
	}
	public void setEntity(RidingLine entity) {
		this.entity = entity;
	}
	public List<LinePoint> getSubEntityList() {
		return subEntityList;
	}
	public void setSubEntityList(List<LinePoint> subEntityList) {
		this.subEntityList = subEntityList;
	}
	
}
