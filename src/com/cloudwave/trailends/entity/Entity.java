package com.cloudwave.trailends.entity;

import java.io.Serializable;

import com.lidroid.xutils.db.annotation.Id;

/**
 * @description 
 * @author DolphinBoy
 * @email dolphinboyo@gmail.com
 * @date 2013-11-9 @time 上午10:14:38
 * TODO
 */

public abstract class Entity implements Serializable {
	private static final long serialVersionUID = -8969254381822921550L;
	
	@Id(column="id_")
	protected Long id;  //ID
	
	public Entity() {
	}
	public Entity(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
