package com.cloudwave.trailends.entity;

import java.io.Serializable;

import com.cloudwave.trailends.domain.User;


/**
 * @Description 
 * @author 龙雪
 * @date 2014年12月23日
 * 主要用于修改用户资料时组织数据
 */

public class UserInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long userId;
	private Integer age;
	/**1:男;2:女;-1:其他;3:未知*/
	private int gender;
	private Integer height;
	private Float weight;
	private String city;
	
	public UserInfoEntity() {
	}
	
	public UserInfoEntity(User u) {
		this.setUserId(u.getId());
		this.setAge(u.getAge());
		this.setGender(u.getGender());
		this.setHeight(u.getHeight());
		this.setWeight(u.getWeight());
		this.setCity(u.getCity());
	}

	public boolean equals(UserInfoEntity other) {
		if (this == other)
			return true;
		if (other == null)
			return false;
		if (age == null) {
			if (other.age != null)
				return false;
		} else if (!age.equals(other.age))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (gender != other.gender)
			return false;
		//目前没有让用户填写身高
//		if (height == null) {
//			if (other.height != null)
//				return false;
//		} else if (!height.equals(other.height))
//			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		if (weight == null) {
			if (other.weight != null)
				return false;
		} else if (!weight.equals(other.weight))
			return false;
		return true;
	}

	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public int getGender() {
		return gender;
	}
	public String getGenderText() {
		if ( gender == 1 ) {
			return "男";
		} else if ( gender == 2 ) {
			return "女";
		} else if ( gender == 3 ) {
			return "其他";
		} else {
			return "未知";
		}
	}
	public void setGenderText(String gender) {
		if ( gender != null ) {
			if ( gender.contains("男") ) {
				this.gender = 1;
			} else if ( gender.contains("女") ) {
				this.gender = 2;
			} else {
				this.gender = -1;
			}
		}
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	public Float getWeight() {
		return weight;
	}
	public void setWeight(Float weight) {
		this.weight = weight;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}

}
