package com.cloudwave.trailends;

/**
 * @description 静态常量
 * @author DolphinBoy
 * 2013年10月11日 上午10:20:34
 * TODO
 */

public interface Constants {
	public static final String APP_NAME = "OnRiding";
	public static final String IS_FIRST_START = "isFirstStart";
	public static final String VERSION_CODE = "version_code";
	public static final String VERSION_NAME = "version_name";
	
	public static final String DB_NAME = "cw_trailends";
	public static final String STATIC_TOKEN = "token";
	public static final String SESSION_USER = "session_user";
	public static final String SESSION_USER_INFO = "session_user_info";
	public static final String SIGNIN_TOKEN = "signin_token";  //登录时返回的Token
	public static final int SESSION_INVALID_TIME  = 30;
	
	public static final int MAX_MILEAGE = 1000;
	
	
	/** 每次加载本地数据的条数 */
	public static final int ONCE_LOAD_LOCAL_COUNT = 20;
	
	public static final int ONCE_LOAD_LOCAL_TRIP_COUNT = 15;
	
	public static final int ONCE_LOAD_LOCAL_TRIP_COMMENT_COUNT = 20;
	
	//	缓存KEY
	public static final String CACHE_TRIPRECORD_COMMENT = "cache_triprecord_comment";  //行记评论
	public static final String CACHE_TRIP_COMMENT = "cache_trip_comment";  //旅程评论
	public static final String CACHE_CYCLING_TEAM_CREATE = "cache_cycling_team_create";  //创建车队
	public static final String CACHE_SEND_TEXT = "cache_send_text";  //发送文字
	
	
	// 视图部分静态常量
	public static final int LISTVIEW_HEAD = 0;
	public static final int LISTVIEW_ITEM = 1;
	
	public static final String CACHE_LAST_REF_TIME = "last_ref_time";
	public static final String CACHE_FEEDBACK_TXT = "feedback_txt";
	public static final String CACHE_USER_NAME = "user_name";
	
	public static final String CACHE_RIDING_LINE_EDIT_TITLE = "riding_line_edit_title";
	public static final String CACHE_RIDING_LINE_EDIT_DESC = "riding_line_edit_desc";
	
	
	
	public static final String WEB_DOMAIN = "http://www.onriding.cc";
	public static final String API_DOMAIN = "http://api.onriding.cc";
//	public static final String API_DOMAIN = "http://192.168.11.120:8080";
	public static final String URL_SINGIN = API_DOMAIN + "/api/sign/signin";
	public static final String URL_SINGUP = API_DOMAIN + "/api/sign/signup";
	public static final String URL_SSO_SINGIN = API_DOMAIN + "/api/sign/sso/signin";
	public static final String URL_SSO_SINGUP = API_DOMAIN + "/api/sign/sso/signup";
	public static final String URL_SSO_SING = API_DOMAIN + "/api/sign/sso/sign";
	
	/**分享跳转链接*/
	// 如果要修改这个变量, 请另外复制一行以记录对应版本使用的地址, 例如:
	// v1.1.0 public static final String SHARE_TARGET_URL = "http://121.40.80.128:8383";
	public static final String SHARE_TARGET_URL = "http://a.app.qq.com/o/simple.jsp?pkgname=com.cloudwave.trailends";
	
	public static final String URL_USER_UPLOAD_TOKEN = API_DOMAIN + "/api/upload/token";
	public static final String URL_USER_AVATAR_UPLOAD = API_DOMAIN + "/api/upload/avatar";
	public static final String URL_USER_INFO_UPDATE = API_DOMAIN + "/api/userinfo/update";
	
//	public static final String URL_RL_CREATE = BASE_URL + "/api/ridingline/create";
	public static final String URL_CRUD_RL = API_DOMAIN + "/api/ridingline";
//	public static final String URL_RL_DELETE = BASE_URL + "/api/ridingline/delete";
	public static final String URL_RIDINGLINE_UPDATE = API_DOMAIN + "/api/ridingline/update";
	
	
	public static final String URL_SYNC_UPLOAD = API_DOMAIN + "/api/sync/upload";
	public static final String URL_SYNC_DOWNLOAD_COUNT = API_DOMAIN + "/api/sync/download/count";
	public static final String URL_SYNC_DOWNLOAD = API_DOMAIN + "/api/sync/download";
	public static final String URL_SYNC_DELETE = API_DOMAIN + "/api/sync/delete";
	
	public static final String URL_FEEDBACK_ADD = API_DOMAIN + "/api/feedback/add";
	
	public static final String SECURITY_QQ_APP_ID = "101106052";
	public static final String SECURITY_QQ_APP_SECRET = "6af1d40137e6f39d3478065816e0fc59";
	
	public static final String SECURITY_WEIXIN_APP_ID = "wxcce306117d7f202d";
	public static final String SECURITY_WEIXIN_APP_SECRET = "fcca02e846e2929e6b7a3007d8711779";
	
	public static final String SECURITY_RENREN_APP_ID = "272527";
	public static final String SECURITY_RENREN_KEY = "9c60b433faf7449dba88ffc9c7ac972f";
	public static final String SECURITY_RENREN_KEY_SECRET = "2e45b0f4958343e498ab9885f6ea4733";
	
}
