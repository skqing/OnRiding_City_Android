package com.cloudwave.trailends.utils;

import java.util.Collection;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @description 
 * @author DolphinBoy
 * @email dolphinboyo@gmail.com
 * @date 2013-8-25 @time 下午1:15:50
 * TODO
 */

public class StringUtils {
//	private static final String FOLDER_SEPARATOR = "/";
//
//	private static final String WINDOWS_FOLDER_SEPARATOR = "\\";
//
//	private static final String TOP_PATH = "..";
//
//	private static final String CURRENT_PATH = ".";
//
//	private static final char EXTENSION_SEPARATOR = '.';

	public static final String PASSWOARD = "[[a-z]|[A-Z]|[0-9]]*";

	public static final String USERNAME = "[[a-z]|[A-Z]|[0-9]]*";

	public static final String NUMBER = "[0-9]*";

	public static final String LETTER = "[[A-Z]|[a-z]]*";

	public static final String PHONE_NO = "0[0-9]{2,3}-[0-9]{7,8}";

	public static final String MOBILE_PHONE_NO = "13[0-9][0-9]{8}";
	
	/**
	 * Convenience method to return a Collection as a delimited (e.g. CSV)
	 * String. E.g. useful for <code>toString()</code> implementations.
	 * 
	 * @param coll
	 *            Collection to display
	 * @param delim
	 *            delimiter to use (probably a ",")
	 * @param prefix
	 *            string to start each element with
	 * @param suffix
	 *            string to end each element with
	 */
	public static String collectionToDelimitedString(Collection<?> coll,
			String delim, String prefix, String suffix) {
		if (coll == null || coll.size() == 0) {
			return "";
		}
		StringBuffer sb = new StringBuffer();
		Iterator<?> it = coll.iterator();
		while (it.hasNext()) {
			sb.append(prefix).append(it.next()).append(suffix);
			if (it.hasNext()) {
				sb.append(delim);
			}
		}
		return sb.toString();
	}
	/**
	 * Convenience method to return a Collection as a delimited (e.g. CSV)
	 * String. E.g. useful for <code>toString()</code> implementations.
	 * 
	 * @param coll
	 *            Collection to display
	 * @param delim
	 *            delimiter to use (probably a ",")
	 */
	public static String collectionToDelimitedString(Collection<?> coll,
			String delim) {
		return collectionToDelimitedString(coll, delim, "", "");
	}

	/**
	 * Convenience method to return a Collection as a CSV String. E.g. useful
	 * for <code>toString()</code> implementations.
	 * 
	 * @param coll
	 *            Collection to display
	 */
	public static String collectionToCommaDelimitedString(Collection<?> coll) {
		return collectionToDelimitedString(coll, ",");
	}
	
	/**
	 * 判断str字符串是否符合我们定义的标准格式，pattern表示格式(正则表达式)。
	 * 
	 * @param str
	 * @param pattern
	 */
	public static boolean isDefinedPattern(String str, String pattern) {

		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);
		return m.matches();
	}
	
	public static String formatFileSize(int size) {
		String ret = "";
		if (size < (1024 * 1024)) {
			ret = String.format("%dK", size / 1024);
		} else {
			ret = String.format("%.1fM", size / (1024 * 1024.0));
		}
		return ret;
	}
}
