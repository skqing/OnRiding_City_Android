package com.cloudwave.trailends.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author DolphinBoy 2013年8月30日 下午2:58:01 正则验证工具类
 */

public class RegexUtils {
	public static final String REGEX_EMAIL = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
	public static final String REGEX_PHONE_NUMBER = "^(13[4,5,6,7,8,9]|15[0,8,9,1,7]|188|187)\\d{8}$";
	// number, character, underline
	public static final String REGEX_NCU = "[^a-zA-Z0-9._-]";

	/**
	 * 是否是邮箱
	 * 
	 * @param email
	 * @return
	 */
	public static boolean isEmail(String email) {
		Pattern regex = Pattern.compile(REGEX_EMAIL);
		Matcher matcher = regex.matcher(email);
		return matcher.matches();
	}

	/**
	 * 是否是手机号码
	 * 
	 * @param phoneNumber
	 * @return
	 */
	public static boolean isMobile(String phoneNumber) {
		Pattern regex = Pattern.compile(REGEX_PHONE_NUMBER);
		Matcher matcher = regex.matcher(phoneNumber);
		return matcher.matches();

	}

	/**
	 * 是否只包含数字字母和下划线
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isNcu(String value) {
		Pattern regex = Pattern.compile(REGEX_NCU);
		Matcher matcher = regex.matcher(value);
		return matcher.matches();
	}

	/**
	 * 过滤掉全部非[数字,字母,下划线]的字符
	 * 
	 * @param value
	 * @return
	 */
	public static String filtUnNcu(String value) {
		Pattern regex = Pattern.compile(REGEX_NCU);
		Matcher matcher = regex.matcher(value);
		return matcher.replaceAll("");
	}

	public static boolean isQQ(String qq) {
		boolean flag = false;
		try {
			String check = "^[1-9][0-9]{4,}$";
			Pattern regex = Pattern.compile(check);
			Matcher matcher = regex.matcher(qq);
			flag = matcher.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	// ************ start ***********/
	/**
	 * @Title:FilterStr.java
	 * @Package:com.you.dao
	 * @Description:Java中过滤数字、字母和中文
	 * @Author: 游海东
	 * @date: 2014年3月12日 下午7:18:20
	 * @Version V1.2.3 http://www.tuicool.com/articles/vU3YVr
	 */

	/**
	 * 
	 * @Title : filterNumber
	 * @Type : FilterStr
	 * @date : 2014年3月12日 下午7:23:03
	 * @Description : 过滤出数字
	 * @param str
	 * @return
	 */
	public static String filterNumber(String number) {
		number = number.replaceAll("[^(0-9)]", "");
		return number;
	}

	/**
	 * 
	 * @Title : filterAlphabet
	 * @Type : FilterStr
	 * @date : 2014年3月12日 下午7:28:54
	 * @Description : 过滤出字母
	 * @param alph
	 * @return
	 */
	public static String filterAlphabet(String alph) {
		alph = alph.replaceAll("[^(A-Za-z)]", "");
		return alph;
	}

	/**
	 * 
	 * @Title : filterChinese
	 * @Type : FilterStr
	 * @date : 2014年3月12日 下午9:12:37
	 * @Description : 过滤出中文
	 * @param chin
	 * @return
	 */
	public static String filterChinese(String chin) {
		chin = chin.replaceAll("[^(\\u4e00-\\u9fa5)]", "");
		return chin;
	}

	/**
	 * 
	 * @Title : filter
	 * @Type : FilterStr
	 * @date : 2014年3月12日 下午9:17:22
	 * @Description : 过滤出字母、数字和中文
	 * @param character
	 * @return
	 */
	public static String filterAll(String character) {
		character = character.replaceAll("[^(a-zA-Z0-9\\u4e00-\\u9fa5)]", "");
		return character;
	}

	public static void main(String[] args) {
		// System.out.println(isEmail("fwe_qq.com"));
		System.out.println(isNcu("fwe_qq.com"));
	}
}
