package com.cloudwave.trailends.utils;

public interface DateFormator {

	public static final String YEAR_MONTH_DAY_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	public static final String yearMonthDayHHMMssSSS = "yyyyMMddHHmmssSSS" ;
	public static final String yearMonthDayHHMMss = "yyyyMMddHHmmss";

	public static final String YEAR_MONTH = "yyyy-MM";
	public static final String SPLIT_CHAR = "-";

	public static final String YEAR_MONTH_DAY = "yyyy-MM-dd";
	
	public static final String YEARMONTHDAY = "yyyyMMdd";

	public static final String MONTH_DAY = "MM-dd";

	public static final String DATE_DOT = "yyyy.MM.dd";
	public static final String DATE_TIME_DOT = "yyyy.MM.dd HH:mm:ss";
	public static final String MINUTE_SECOND = "HH:mm";
	
	public static final int ONE_SECOND = 1000;  //一秒钟
	public static final int ONE_MINUTE = ONE_SECOND * 60;  //一分钟
	public static final int ONE_HOUR = ONE_MINUTE * 60;  //一小时
	public static final int ONE_DAY = ONE_HOUR * 24;  //一天
	
}
