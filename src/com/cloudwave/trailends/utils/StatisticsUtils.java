package com.cloudwave.trailends.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.lidroid.xutils.http.RequestParams;

/**
 * @Description 统计工具类
 * @author 龙雪
 * @date 2014年12月19日
 * 
 */

public class StatisticsUtils {

	private Context context;

	@SuppressWarnings("unused")
	private String IMEI; // imel
	private int versionCode; // 版本号
	@SuppressWarnings("unused")
	private String MAC;
	private String MODEL; // 手机型号
	private String umengChannel; // 渠道号

	private static StatisticsUtils statisticsUtils;

	public static StatisticsUtils newInstance(Context mContext) {
		if (statisticsUtils == null) {
			statisticsUtils = new StatisticsUtils(mContext);
		}
		return statisticsUtils;
	}

	private StatisticsUtils(Context context) {
		this.context = context;
		MODEL = Build.VERSION.RELEASE;
		try {
			versionCode = getVersionCode();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		ApplicationInfo appInfo;
		try {
			appInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(),PackageManager.GET_META_DATA);
			umengChannel = appInfo.metaData.getString("UMENG_CHANNEL");
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		
		IMEI = ((TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		MAC = getLocalMacAddress(context);
	}

	public RequestParams appendParams(String apiVer, RequestParams params) {
		if ( params == null ) {
			params = new RequestParams();
		}
		params.addHeader("api-version", apiVer);
		params.addBodyParameter("cid", umengChannel);
		params.addBodyParameter("versionCode", String.valueOf(versionCode));
		params.addBodyParameter("MODEL", MODEL);
//		params.addQueryStringParameter("IMEI", IMEI);
//		params.addQueryStringParameter("MAC", MAC);
		
		return params;
	}
	
	/**
	 * 获取MAC地址
	 * 
	 * @param context
	 * @return
	 */
	private String getLocalMacAddress(Context context) {
		WifiManager wifi = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);
		WifiInfo info = wifi.getConnectionInfo();
		if (info != null) {
			return info.getMacAddress();
		}
		return null;
	}

	public String getModel() {
		return MODEL;
	}
	
	public int getVersionCode() {
		return getPackageInfo().versionCode;
	}

	public String getVersionName() {
		return getPackageInfo().versionName;
	}

	/**
	 * 获取App安装包信息
	 * 
	 * @return
	 */
	public PackageInfo getPackageInfo() {
		PackageInfo info = null;
		try {
			info = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			Toast.makeText(context, "获取应用信息失败~_~", Toast.LENGTH_SHORT).show();
		}

		if (info == null) {
			info = new PackageInfo();
		}
		return info;
	}

}
