package com.cloudwave.trailends.utils;

import java.util.Collection;

/**
 * 集合工具类
 * @author DolphinBoy
 *
 */

public class CollectionUtils {

	public static boolean isEmpty(Collection<?> coll) {
        return (coll == null || coll.isEmpty());
    }
	
	public static boolean isNotEmpty(Collection<?> coll) {
        return !CollectionUtils.isEmpty(coll);
    }
	
}
