package com.cloudwave.trailends.widget.view;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.cloudwave.trailends.R;

/**
 * 弹框loading 目前封装的不够好
 * 
 * @author 龙雪
 * 
 */

public class LoadingDialog extends DialogFragment {

	private String msg = "Loading";

	public LoadingDialog() {
		super();
	}

	public LoadingDialog(String msg) {
		super();
		this.msg = msg;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.dialog_loading, null);
		TextView title = (TextView) view
				.findViewById(R.id.id_dialog_loading_msg);
		title.setText(msg);
		Dialog dialog = new Dialog(getActivity(), R.style.dialog);
		dialog.setContentView(view);
		return dialog;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
