package com.cloudwave.trailends.widget.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cloudwave.trailends.R;

/**
 * 图片文字 组合控件
 * @author DolphinBoy
 * @email dolphinboyo@gmail.com
 * @date 2014-5-11
 * @time 下午8:49:22
 * TODO
 */

public class ImgBtn extends LinearLayout {
	private ImageView iv;
    private TextView  tv;
    
    
    public ImgBtn(Context context) {
        this(context, null);
    }

    public ImgBtn(Context context, AttributeSet attrs) {
        super(context, attrs);
        // 导入布局
        LayoutInflater.from(context).inflate(R.layout.custom_imgbtn_stat, this, true);
        iv = (ImageView) findViewById(R.id.iv);
        tv = (TextView) findViewById(R.id.tv);

    }

    /**
     * 设置图片资源
     */
    public void setImageResource(int resId) {
        iv.setImageResource(resId);
    }

    /**
     * 设置显示的文字
     */
    public void setTextViewText(String text) {
        tv.setText(text);
    }
}
