package com.cloudwave.trailends.widget.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cloudwave.trailends.R;

/**
 * <p>
 * Title: CustomDialog
 * </p>
 * <p>
 * Description:自定义Dialog（参数传入Dialog样式文件，Dialog布局文件）
 * </p>
 * <p>
 * Copyright: Copyright (c) 2013
 * </p>
 * 
 * @author archie
 * @version 1.0
 */
public class CustomDialog extends Dialog {
	private int layoutRes;// 布局文件
	private Context context;
	/** 确定按钮 **/
	private Button confirmBtn;
	/** 取消按钮 **/
	private Button cancelBtn;
	private TextView tvContent;
	
	private View.OnClickListener onClickListener;
	
	private int resId;
	private boolean showOkBtn = true;
	
	public CustomDialog(Context context, View.OnClickListener onClickListener) {
		super(context);
		this.context = context;
		this.onClickListener = onClickListener;
	}

	/**
	 * 自定义布局的构造方法
	 * 
	 * @param context
	 * @param resLayout
	 */
	public CustomDialog(Context context, int resLayout, View.OnClickListener onClickListener) {
		super(context);
		this.context = context;
		this.layoutRes = resLayout;
		this.onClickListener = onClickListener;
	}

	/**
	 * 自定义主题及布局的构造方法
	 * 
	 * @param context
	 * @param theme
	 * @param resLayout
	 */
	public CustomDialog(Context context, int theme, int resLayout, View.OnClickListener onClickListener) {
		super(context, theme);
		this.context = context;
		this.layoutRes = resLayout;
		this.onClickListener = onClickListener;
	}

	/**
	 * 自定义主题,内容及布局的构造方法
	 * 
	 * @param context
	 * @param theme
	 * @param resLayout
	 */
	public CustomDialog(Context context, int theme, int resLayout, int resId, View.OnClickListener onClickListener) {
		super(context, theme);
		this.context = context;
		this.layoutRes = resLayout;
		this.resId = resId;
		this.onClickListener = onClickListener;
	}
	
	/**
	 * 自定义主题,内容及布局的构造方法
	 * 
	 * @param context
	 * @param theme
	 * @param resLayout
	 */
	public CustomDialog(Context context, int theme, int resLayout, int resId, boolean showOkBtn) {
		super(context, theme);
		this.context = context;
		this.layoutRes = resLayout;
		this.resId = resId;
		this.showOkBtn = showOkBtn;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(layoutRes);
		
		// 根据id在布局中找到控件对象
		confirmBtn = (Button) findViewById(R.id.confirm_btn);
		cancelBtn = (Button) findViewById(R.id.cancel_btn);
		tvContent = (TextView) findViewById(R.id.tvContent);
		
		// 设置按钮的文本颜色
		confirmBtn.setTextColor(0xff1E90FF);
		cancelBtn.setTextColor(0xff1E90FF);
		
		tvContent.setText(resId);
		
		if ( !showOkBtn ) {
			confirmBtn.setVisibility(View.GONE);
			View split = findViewById(R.id.btn_split);
			split.setVisibility(View.GONE);
		}
		// 为按钮绑定点击事件监听器
//		confirmBtn.setOnClickListener(onClickListener);
		confirmBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if ( onClickListener != null ){
					onClickListener.onClick(v);
				}
				CustomDialog.this.dismiss();
			}
		});
//		cancelBtn.setOnClickListener(onClickListener);
		cancelBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				CustomDialog.this.dismiss();
			}
		});
	}

}