package com.cloudwave.trailends.adapter;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class RidingLineHolder {
	public View llListItem;
	public TextView idValue;
	public TextView title;
	public TextView mileage;
	public TextView point;
	public TextView time;
	public Button status;
	public ImageView tvFlag;
	public View rlMenu;
	public TextView detail;
	public TextView delete;
}
