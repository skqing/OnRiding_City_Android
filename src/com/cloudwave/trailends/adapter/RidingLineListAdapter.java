package com.cloudwave.trailends.adapter;

import java.text.DecimalFormat;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudwave.trailends.Constants;
import com.cloudwave.trailends.R;
import com.cloudwave.trailends.domain.RidingLine;
import com.cloudwave.trailends.utils.DateUtils;
import com.lidroid.xutils.DbUtils;

/**
 * @author DolphinBoy
 * 2013年10月23日 下午4:15:29
 * 
 */

public class RidingLineListAdapter extends BaseAdapter {
	private Context context;// 运行上下文
	private List<RidingLine> tList;
	private LayoutInflater listContainer;// 视图容器
	private OnDeleteAdapterItemListener onDeleteAdapterItemListener;
//	private float x, ux;

	public RidingLineListAdapter(Context context, List<RidingLine> tList) {
		this.context = context;
		this.tList = tList;
		this.listContainer = LayoutInflater.from(context); // 创建视图容器并设置上下文
	}

	@Override
	public int getCount() {
		return tList.size();
	}

	@Override
	public Object getItem(int position) {
		return tList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RidingLineHolder tHolder = null;

		if (convertView == null) {
			convertView = listContainer.inflate(R.layout.trip_list_item, null);

			tHolder = new RidingLineHolder();

			tHolder.llListItem =  convertView
					.findViewById(R.id.ll_list_item);
			tHolder.idValue = (TextView) convertView
					.findViewById(R.id.t_item_idvalue_id);
			tHolder.title = (TextView) convertView
					.findViewById(R.id.t_item_title_id);
			tHolder.mileage = (TextView) convertView
					.findViewById(R.id.t_item_mileage_id);
			tHolder.point = (TextView) convertView
					.findViewById(R.id.t_item_point_id);
			tHolder.time = (TextView) convertView
					.findViewById(R.id.t_item_time_id);
//			tHolder.zanSum.setOnClickListener(new dealClickListener(position));
			tHolder.tvFlag = (ImageView) convertView
					.findViewById(R.id.tv_flag);
			
			tHolder.rlMenu =  convertView
					.findViewById(R.id.rl_list_item_menu);
			tHolder.detail = (TextView) convertView
					.findViewById(R.id.tv_menu_btn_detail);
			tHolder.delete = (TextView) convertView
					.findViewById(R.id.tv_menu_btn_delete);
			
			// 设置控件集到convertView
			convertView.setTag(tHolder);
		} else {
			tHolder = (RidingLineHolder) convertView.getTag();
		}

		final RidingLine t = tList.get(position);
		
		if ( t.isNow() ) {
			tHolder.tvFlag.setBackgroundResource(R.drawable.icon_on_riding);
		} else {
			if ( t.getEndTime() == null ) {
				tHolder.tvFlag.setBackgroundResource(R.drawable.icon_no_finish);
			} else {
				if ( t.getSendStatus() == 0 ) {
					tHolder.tvFlag.setBackgroundResource(R.drawable.icon_no_sync);
				} else {
					tHolder.tvFlag.setVisibility(View.INVISIBLE);
				}
			}
		}
		
		tHolder.idValue.setText(String.valueOf(t.getId()));
		tHolder.title.setText(t.getTitle());
		DecimalFormat df = new DecimalFormat("#0.00");
		tHolder.mileage.setText(df.format(t.getTotalMileage() / 1000));
		tHolder.point.setText(String.valueOf(t.getTotalPoint()));
		
		String tStr = context.getResources().getString(R.string.stat_times);
        int[] time = DateUtils.duration(t.getTotalTime() * 1000);
        String tNewStr = String.format(tStr, time[1], time[2], time[3]); 
		tHolder.time.setText(tNewStr);
		
//		convertView.setOnTouchListener(new OnTouchListener() {
//			
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				
//				final RidingLineHolder ridingLineHolder = (RidingLineHolder) v.getTag();
//				
//				if (event.getAction() == MotionEvent.ACTION_DOWN) {
//					Toast.makeText(context, "ACTION_DOWN...", Toast.LENGTH_SHORT).show();
//					v.setBackgroundResource(R.color.them_gray);
//					x = event.getX();  // 获取按下时的x轴坐标
//				} else if (event.getAction() == MotionEvent.ACTION_UP) {
//					v.setBackgroundResource(R.color.bg_white);  // 获取松开时的x坐标
//					ux = event.getX();
//					float r = x - ux;
//					Toast.makeText(context, "ACTION_UP..."+r, Toast.LENGTH_SHORT).show();
//					// 按下和松开绝对值差当大于20时显示删除按钮，否则不显示
//					if (r < -20) {
//						Toast.makeText(context, "你滑动了...", Toast.LENGTH_SHORT).show();
//						ridingLineHolder.rlMenu.setVisibility(View.VISIBLE);
////							tHolder.rlMenu.setVisibility(View.VISIBLE);
//					} else if (r > 20) {
//						
//					} else if (Math.abs(r) < 20) {
//						Intent intent = new Intent(context, PathViewActivity.class);
//						intent.putExtra("rlId", t.getId());
//						context.startActivity(intent);
//					}
//				} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
//					v.setBackgroundResource(R.color.them_gray);
//				}  else {
//					v.setBackgroundResource(R.color.bg_white);
//				}
//				return true;
//			}
//		});
		final int p = position;
		tHolder.delete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				deleteItem(view, p, t.getId());
			}
		});
		return convertView;
	}

	private void deleteItem(View view, int position, Long id) {
		DbUtils dbu = DbUtils.create(this.context, Constants.DB_NAME);
		try {
			dbu.deleteById(RidingLine.class, id);
			if (onDeleteAdapterItemListener != null) {
				onDeleteAdapterItemListener.onDelete(view, position, id, true);
			}
		} catch (Exception e) {
			if (onDeleteAdapterItemListener != null) {
				onDeleteAdapterItemListener.onDelete(view, position, id, false);
			}
			Toast.makeText(context, "删除失败~_~", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void setOnDeleteAdapterItemListener(
			OnDeleteAdapterItemListener onDeleteAdapterItemListener) {
		this.onDeleteAdapterItemListener = onDeleteAdapterItemListener;
	}
	
}