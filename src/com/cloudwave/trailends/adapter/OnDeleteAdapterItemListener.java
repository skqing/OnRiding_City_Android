package com.cloudwave.trailends.adapter;

/**
 * 
 * @author DolphinBoy
 * @date 2014年9月29日
 * TODO
 */

public interface OnDeleteAdapterItemListener {

	public abstract void onDelete(android.view.View view, int position, Object objId, boolean isSuccess);
}
