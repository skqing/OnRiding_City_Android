package com.cloudwave.trailends.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.baidu.mapapi.map.offline.MKOLSearchRecord;
import com.cloudwave.trailends.R;
import com.cloudwave.trailends.utils.StringUtils;

public class HotCityAdapter extends BaseAdapter {
	
	private Context context;// 运行上下文
	private List<MKOLSearchRecord> cityList;
	private LayoutInflater listContainer;// 视图容器
//	private OnDeleteAdapterItemListener onDeleteAdapterItemListener;

	class HotCityHolder {
		public TextView tvCityName;
		public TextView tvSize;
		
	}
	
	public HotCityAdapter(Context context, List<MKOLSearchRecord> cityList) {
		this.context = context;
		this.cityList = cityList;
		this.listContainer = LayoutInflater.from(context); // 创建视图容器并设置上下文
	}

	@Override
	public int getCount() {
		return cityList.size();
	}

	@Override
	public Object getItem(int position) {
		return cityList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		HotCityHolder cHolder = null;

		if (convertView == null) {
			convertView = listContainer.inflate(R.layout.hot_city_grid_item, null);

			cHolder = new HotCityHolder();

			cHolder.tvCityName = (TextView) convertView
					.findViewById(R.id.tvCityName);
			cHolder.tvSize = (TextView) convertView
					.findViewById(R.id.tvSize);
			
			// 设置控件集到convertView
			convertView.setTag(cHolder);
		} else {
			cHolder = (HotCityHolder) convertView.getTag();
		}

		MKOLSearchRecord t = cityList.get(position);
		
		cHolder.tvCityName.setText(t.cityName);
		cHolder.tvSize.setText(StringUtils.formatFileSize(t.size));
		
		return convertView;
	}
	
	
}
