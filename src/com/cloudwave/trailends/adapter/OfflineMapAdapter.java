package com.cloudwave.trailends.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.baidu.mapapi.map.offline.MKOLUpdateElement;
import com.cloudwave.trailends.R;
import com.cloudwave.trailends.ui.MapViewActivity;
import com.cloudwave.trailends.ui.OfflineMapActivity;
import com.cloudwave.trailends.utils.StringUtils;

public class OfflineMapAdapter extends BaseAdapter {

	private Context context;// 运行上下文
	List<MKOLUpdateElement> offlineList;
	private LayoutInflater listContainer;// 视图容器
//	private OnDeleteAdapterItemListener onDeleteAdapterItemListener;

	class OfflineCityHolder {
		public TextView tvCityName;
		public TextView tvCitySize;
		public TextView tvStat;
		
		public ProgressBar pbDownload;
		public TextView tvView;
		public TextView tvUpdate;
		public TextView tvDelete;
	}
	
	public OfflineMapAdapter(Context context, List<MKOLUpdateElement> offlineList) {
		this.context = context;
		this.offlineList = offlineList;
		this.listContainer = LayoutInflater.from(context); // 创建视图容器并设置上下文
	}

	@Override
	public int getCount() {
		return offlineList.size();
	}

	@Override
	public Object getItem(int position) {
		return offlineList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		OfflineCityHolder cHolder = null;

		if (convertView == null) {
			convertView = listContainer.inflate(R.layout.map_offline_list_item, null);

			cHolder = new OfflineCityHolder();

			cHolder.tvCityName = (TextView) convertView
					.findViewById(R.id.tvCityName);
			cHolder.tvCitySize = (TextView) convertView
					.findViewById(R.id.tvCitySize);
			cHolder.tvStat = (TextView) convertView
					.findViewById(R.id.tvStat);
			cHolder.pbDownload = (ProgressBar) convertView
					.findViewById(R.id.pbDownload);
			cHolder.tvView = (TextView) convertView
					.findViewById(R.id.tvView);
			cHolder.tvUpdate = (TextView) convertView
					.findViewById(R.id.tvUpdate);
			cHolder.tvDelete = (TextView) convertView
					.findViewById(R.id.tvDelete);
			
			// 设置控件集到convertView
			convertView.setTag(cHolder);
		} else {
			cHolder = (OfflineCityHolder) convertView.getTag();
		}

		final MKOLUpdateElement t = offlineList.get(position);
		
		String size = context.getResources().getString(R.string.offline_map_city_size);
        String tNewSize = String.format(size, StringUtils.formatFileSize(t.serversize));
        cHolder.tvCitySize.setText(tNewSize);
        if ( t.ratio != 100 ) {
        	cHolder.tvView.setEnabled(false);
        	cHolder.pbDownload.setVisibility(View.VISIBLE);
        	cHolder.pbDownload.setMax(100);
            cHolder.pbDownload.setProgress(t.ratio);
            cHolder.tvUpdate.setEnabled(true);
            if ( t.status == MKOLUpdateElement.SUSPENDED ) {
            	cHolder.tvStat.setText("暂停下载("+t.ratio + "%)");
            	cHolder.tvUpdate.setText("继续下载");
            } else {
            	cHolder.tvUpdate.setText("暂停下载");
            	cHolder.tvStat.setText("下载中("+t.ratio + "%)");
            }
        } else {
        	cHolder.tvView.setEnabled(true);
        	cHolder.pbDownload.setVisibility(View.GONE);
        	cHolder.tvStat.setText("已下载");
        	
        	if ( t.update ) {
        		cHolder.tvUpdate.setText("更新地图");
            	cHolder.tvUpdate.setEnabled(true);
        	} else {
        		cHolder.tvUpdate.setText("更新地图");
        		cHolder.tvUpdate.setEnabled(false);
        	}
        }
        
		cHolder.tvCityName.setText(t.cityName);
		
		cHolder.tvView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if ( t.ratio == 100 ) {
					Intent intent = new Intent();
					intent.putExtra("action", "view");
					intent.putExtra("x", t.geoPt.longitude);
					intent.putExtra("y", t.geoPt.latitude);
					intent.setClass(context, MapViewActivity.class);
					context.startActivity(intent);
				}
			}
		});
		
		cHolder.tvUpdate.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if ( t.ratio != 100 ) {
					if ( t.status == MKOLUpdateElement.SUSPENDED ) {
						((OfflineMapActivity) context).start(t.cityID);
					} else {
						((OfflineMapActivity) context).stop(t.cityID);
					}
				} else {
					if ( t.update ) {
						((OfflineMapActivity) context).start(t.cityID);
		        	}
				}
			}
		});
		
		cHolder.tvDelete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((OfflineMapActivity) context).remove(t.cityID);
			}
		});
		
		return convertView;
	}
	
}
