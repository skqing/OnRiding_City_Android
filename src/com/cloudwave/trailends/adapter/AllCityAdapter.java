package com.cloudwave.trailends.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.baidu.mapapi.map.offline.MKOLSearchRecord;
import com.cloudwave.trailends.R;
import com.cloudwave.trailends.utils.CollectionUtils;
import com.cloudwave.trailends.utils.StringUtils;

public class AllCityAdapter extends BaseExpandableListAdapter {

	private Context context;// 运行上下文
	private List<MKOLSearchRecord> cityList;
	private LayoutInflater listContainer;// 视图容器
//	private OnDeleteAdapterItemListener onDeleteAdapterItemListener;

	class AllCityHolder {
		public View rlAllCityItem;
		public ImageView ivGroupFlag;
		public TextView tvCityName;
		public TextView tvSize;
		
	}
	
	public AllCityAdapter(Context context, List<MKOLSearchRecord> cityList) {
		this.context = context;
		this.cityList = cityList;
		this.listContainer = LayoutInflater.from(context); // 创建视图容器并设置上下文
	}

	@Override
	public int getGroupCount() {
		return cityList.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if (cityList.get(groupPosition).childCities == null) {
			return 0;
		} else {
			return cityList.get(groupPosition).childCities.size();
		}
	}

	@Override
	public Object getGroup(int groupPosition) {
		return getGroup(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return cityList.get(groupPosition).childCities.get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return cityList.get(groupPosition).cityID;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return cityList.get(groupPosition).childCities.get(childPosition).cityID;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		AllCityHolder cHolder = null;
		if (convertView == null) {
			convertView = listContainer.inflate(R.layout.all_city_list_item, null);
			cHolder = new AllCityHolder();
			
			cHolder.rlAllCityItem = (View) convertView
					.findViewById(R.id.rlAllCityItem);
			cHolder.ivGroupFlag = (ImageView) convertView
					.findViewById(R.id.ivGroupFlag);
			cHolder.tvCityName = (TextView) convertView
					.findViewById(R.id.tvCityName);
			cHolder.tvSize = (TextView) convertView
					.findViewById(R.id.tvSize);
			
			// 设置控件集到convertView
			convertView.setTag(cHolder);
		} else {
			cHolder = (AllCityHolder) convertView.getTag();
		}
		
		final MKOLSearchRecord t = cityList.get(groupPosition);
		
		cHolder.tvCityName.setText(t.cityName);
		cHolder.tvSize.setText(StringUtils.formatFileSize(t.size));
		
		if ( CollectionUtils.isNotEmpty(t.childCities)) {
			cHolder.ivGroupFlag.setVisibility(View.VISIBLE);
//			cHolder.rlAllCityItem.setBackgroundResource(R.color.bg_gray);
			if ( isExpanded ) {
				cHolder.ivGroupFlag.setImageResource(R.drawable.qb_qrcode_menu_up);
			} else {
				cHolder.ivGroupFlag.setImageResource(R.drawable.qb_qrcode_menu_down);
			}
		} else {
			cHolder.ivGroupFlag.setVisibility(View.GONE);
//			cHolder.rlAllCityItem.setBackgroundResource(R.color.bg_white);
		}

		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		
		AllCityHolder cHolder = null;
		if (convertView == null) {
			convertView = listContainer.inflate(R.layout.all_city_sub_list_item, null);
			cHolder = new AllCityHolder();
			
			cHolder.tvCityName = (TextView) convertView
					.findViewById(R.id.tvCityName);
			cHolder.tvSize = (TextView) convertView
					.findViewById(R.id.tvSize);
			
			// 设置控件集到convertView
			convertView.setTag(cHolder);
		} else {
			cHolder = (AllCityHolder) convertView.getTag();
		}
		
		MKOLSearchRecord t = cityList.get(groupPosition).childCities.get(childPosition);
		
		cHolder.tvCityName.setText(t.cityName);
		cHolder.tvSize.setText(StringUtils.formatFileSize(t.size));
		
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
	
}
